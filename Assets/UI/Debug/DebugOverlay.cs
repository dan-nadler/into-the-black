using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugOverlay : MonoBehaviour
{

    Canvas Canvas;
    // Start is called before the first frame update
    void Start()
    {
        Canvas = GetComponent<Canvas>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.D))
        {
            Canvas.enabled = !Canvas.enabled;
        }   
    }


}
