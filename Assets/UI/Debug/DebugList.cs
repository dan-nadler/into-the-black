using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugList : MonoBehaviour
{
    public Dictionary<string, string> Messages = new Dictionary<string, string>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Text>().text = "";
        foreach (string value in Messages.Values)
        {
            this.GetComponent<Text>().text += value + "\n";
        }
    }
}
