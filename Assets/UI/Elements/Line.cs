using System;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

namespace UI.Elements
{
    [RequireComponent(typeof(LineRenderer))]
    public class Line : MonoBehaviour
    {
        private LineRenderer _line;
        public List<Vector3> points = new List<Vector3>();
        public bool dynamic = false;

        private void OnEnable()
        {
            _line = gameObject.GetComponent<LineRenderer>();
            _line.useWorldSpace = true;
            _line.positionCount = points.Count;
            CreatePoints();
        }

        private void Update()
        {
            if (dynamic) CreatePoints();
        }

        public void CreatePoints()
        {
            _line.positionCount = points.Count;
            for (int i = 0; i < points.Count; i++)
            {
                _line.SetPosition(i, points[i]);    
            }
        }
    }
}
