using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TabGroup : MonoBehaviour
{
    private Tab[] tabs;
    public GameObject pagesContainer;

    private void Awake()
    {
        EnablePage(0);
    }

    public void Subscribe(Tab tab)
    {
        tabs ??= Array.Empty<Tab>();
        tabs.Append(tab);
        Debug.Log(tabs);
    }

    public void EnablePage(int i)
    {
        for (int j = 0; j < pagesContainer.transform.childCount; j++)
        {
            pagesContainer.transform.GetChild(j).gameObject.SetActive(i == j);    
        }
    }
}
