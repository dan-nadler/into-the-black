using UnityEngine;
using UnityEngine.EventSystems;

public class Tab : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public GameObject tabGroup;
    
    void Start()
    {
        tabGroup.GetComponent<TabGroup>().Subscribe(this);
    }

    void Update()
    {
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        // throw new System.NotImplementedException("OnPointerEnter");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        // throw new System.NotImplementedException("OnPointerExit");
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        tabGroup.GetComponent<TabGroup>().EnablePage(this.transform.GetSiblingIndex());
    }
}
