using System;
using UnityEngine;

namespace UI.Elements
{
    [RequireComponent(typeof(LineRenderer))]
    public class CircleOutline : MonoBehaviour
    {
        public enum Planes { XY, XZ, YZ }
        [Range(0, 500)]
        public int segments = 50;
        [Range(0, 1000)]
        public float radius = 5;

        public Planes CirclePlane = Planes.XZ;
        LineRenderer line;

        private float LerpStart = 0;

        public bool dynamic = false;

        void Start()
        {
            line = gameObject.GetComponent<LineRenderer>();

            line.positionCount = segments + 1;
            line.useWorldSpace = false;
            CreatePoints();
        }

        private void Update()
        {
            if (dynamic) CreatePoints();
        }

        void CreatePoints()
        {
            float x;
            float y;

            float angle = 20f;

            for (int i = 0; i < (segments + 1); i++)
            {
                x = Mathf.Sin(Mathf.Deg2Rad * angle) * radius;
                y = Mathf.Cos(Mathf.Deg2Rad * angle) * radius;

                switch (CirclePlane)
                {
                    case Planes.XY:
                        line.SetPosition(i, new Vector3(x, y, 0));
                        break;
                    case Planes.XZ:
                        line.SetPosition(i, new Vector3(x, 0, y));
                        break;
                    case Planes.YZ:
                        line.SetPosition(i, new Vector3(0, x, y));
                        break;
                }
            
                angle += (360f / segments);
            }
        }
    }
}
