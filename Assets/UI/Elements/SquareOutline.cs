using System;
using UnityEngine;

namespace UI.Elements
{
    [RequireComponent(typeof(LineRenderer))]
    public class SquareOutline : MonoBehaviour
    {
        public enum Planes { XY, XZ, YZ }
        
        [Range(0, 1000)]
        public float width = 5;

        public Planes plane = Planes.XZ;
        LineRenderer line;

        void Start()
        {
            line = gameObject.GetComponent<LineRenderer>();

            line.positionCount = 5;
            line.useWorldSpace = false;
            CreatePoints();
        }

        void CreatePoints()
        {
            float x;
            float y;
            int i = 0;

            Tuple<float, float>[] points = new Tuple<float, float>[5];
            points[0] = new Tuple<float, float>(0f, 0f);
            points[1] = new Tuple<float, float>(1f, 0f);
            points[2] = new Tuple<float, float>(1f, 1f);
            points[3] = new Tuple<float, float>(0f, 1f);
            points[4] = new Tuple<float, float>(0f, 0f);

            foreach (Tuple<float, float> p in points)
            {
                x = p.Item1 * width - (width/2f);
                y = p.Item2 * width - (width/2f);
                
                switch (plane)
                {
                    case Planes.XY:
                        line.SetPosition(i, new Vector3(x, y, 0));
                        break;
                    case Planes.XZ:
                        line.SetPosition(i, new Vector3(x, 0, y));
                        break;
                    case Planes.YZ:
                        line.SetPosition(i, new Vector3(0, x, y));
                        break;
                }

                i++;
            }
        }
    }
}
