using UnityEngine;
using UnityEngine.EventSystems;

public class DecreaseZoom : MonoBehaviour, IPointerClickHandler
{
    public CameraControl cameraControl;
    public void OnPointerClick(PointerEventData eventData)
    {
        cameraControl.DecreaseCameraZoom();
    }
}
