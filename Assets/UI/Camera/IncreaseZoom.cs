using UnityEngine;
using UnityEngine.EventSystems;

public class IncreaseZoom : MonoBehaviour, IPointerClickHandler
{
    public CameraControl cameraControl;
    public void OnPointerClick(PointerEventData eventData)
    {
        cameraControl.IncreaseCameraZoom();
    }
}
