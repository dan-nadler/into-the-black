﻿using System;
using UnityEngine;

public class LookAtMainCamera : MonoBehaviour
{
    public bool lockScale = false;
    public Vector3 unitScale;

    private Vector3 ComputeScale()
    {
        if (UnityEngine.Camera.main is { })
        {
            float distanceToCamera =
                Mathf.Abs((UnityEngine.Camera.main.transform.position - transform.position).magnitude);
            float x = distanceToCamera / unitScale.x;
            float y = distanceToCamera / unitScale.y;
            float z = distanceToCamera / unitScale.z;
            return new Vector3(x, y, z);
        }

        throw new Exception("No main camera");
    }

    void Update()
    {
        if (UnityEngine.Camera.main is { }) this.transform.LookAt(UnityEngine.Camera.main.transform.position);

        if (lockScale)
        {
            this.transform.localScale = ComputeScale();
        }
    }
}