using System;
using System.Collections.Generic;
using CameraControls;
using Stations;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

namespace CameraControls
{
    public enum ZoomLevel
    {
        Station,
        Planet,
        Region,
        System,
        DeepSystem,
        Constellation
    }


    public static class Constants
    {
        public static Dictionary<ZoomLevel, float> ZoomDistance = new Dictionary<ZoomLevel, float>()
        {
            {ZoomLevel.Station, 50},
            {ZoomLevel.Planet, 250},
            {ZoomLevel.Region, 800},
            {ZoomLevel.System, 2400},
            {ZoomLevel.DeepSystem, 10000},
            {ZoomLevel.Constellation, 25000},
        };
    }

    public static class Methods
    {
        public static Vector3 GetRelativePosition(Transform focus, Camera camera)
        {
            return focus.transform.position - camera.transform.position;
        }

        public static void FocusCamera(ZoomLevel zoomLevel, Transform focus, Camera camera)
        {
            float dist = CameraControls.Constants.ZoomDistance[zoomLevel];
            FocusCamera(dist, focus, camera);
        }

        public static void FocusCamera(float distance, Transform focus, Camera camera)
        {
            var relativePosition = DistanceToFocus(focus, camera);
            camera.transform.Translate(new Vector3(0, 0, relativePosition.magnitude - distance));
            camera.transform.LookAt(focus);
        }

        public static Vector3 DistanceToFocus(Transform focus, Camera camera)
        {
            Vector3 relativePosition = focus.transform.position - camera.transform.position;
            return relativePosition;
        }

        public static void TranslateAround(Vector3 movement, Transform focus, Camera camera)
        {
            if ((GetRelativePosition(focus, camera) + movement).magnitude >= 10) // 10 is the minimum distance to focus object
            {
                camera.transform.Translate(movement);
                camera.transform.LookAt(focus);
            }
        }

        public static void SetCameraAsChild(Transform focus, Camera camera)
        {
            camera.transform.parent = focus;
        }

        public static void SetCameraParentToRoot(Camera camera)
        {
            camera.transform.parent = null;
        }
    }
}

public class CameraControl : MonoBehaviour
{
    private Vector3 MouseStart;
    private Camera _camera;
    public Transform focus;

    private float _lastClick;
    public float doubleClickDelay = 0.5f;

    private void Start()
    {
        _camera = Camera.main;
        Methods.FocusCamera(CameraControls.ZoomLevel.Station, this.focus, _camera);
        _lastClick = 0f;
    }

    void Update()
    {
        FocusOnDoubleClick();
        CameraMovement();
    }
    
    private void FocusOnDoubleClick()
    {
        if (Input.GetMouseButtonDown((int)MouseButton.LeftMouse) && !EventSystem.current.IsPointerOverGameObject())
        {
            if (Time.time - _lastClick <= doubleClickDelay)
            {
                if (Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out RaycastHit hit))
                {
                    if(hit.transform.TryGetComponent(out StationBehavior sb))
                    {
                        SetCameraFocus(sb.transform);
                        return;
                    }
                }
            }
            
            _lastClick = Time.time;
        }
    }
    
    private float angluarDelta() =>
        Mathf.Tan(5f / 180f) * Vector3.Distance(_camera.transform.position, focus.transform.position);

    private void CameraMovement()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            CameraControls.Methods.TranslateAround(new Vector3(1, 0, 0) * angluarDelta(), focus, _camera);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            CameraControls.Methods.TranslateAround(new Vector3(-1, 0, 0) * angluarDelta(), focus, _camera);
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            CameraControls.Methods.TranslateAround(new Vector3(0, 1, 0) * angluarDelta(), focus, _camera);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            CameraControls.Methods.TranslateAround(new Vector3(0, -1, 0) * angluarDelta(), focus, _camera);
        }
    }

    public void SetCameraFocus(Transform transform)
    {
        this.focus.position = transform.position;
        _camera.transform.LookAt(this.focus);
    }
    
    public void SetCameraFocus(Transform[] transforms)
    {
        Vector3 position = Vector3.zero;
        foreach (Transform t in transforms) position += t.position;
        position /= transforms.Length;
        
        this.focus.position = position;
        _camera.transform.LookAt(this.focus);
    }
    
    public void SetCameraFocus(GameObject[] gos)
    {
        Vector3 position = Vector3.zero;
        foreach (GameObject go in gos) position += go.transform.position;
        position /= gos.Length;
        
        this.focus.position = position;
        _camera.transform.LookAt(this.focus);
    }
    
    public void SetCameraFocus(Vector3 position)
    {
        this.focus.position = position;
        _camera.transform.LookAt(this.focus);
    }

    public void SetCameraFocus(Transform transform, float distance)
    {
        this.focus.position = transform.position;
        _camera.transform.LookAt(this.focus);
        CameraControls.Methods.FocusCamera(distance, this.focus, Camera.main);
    }

    public void SetCameraFocus(Transform transform, CameraControls.ZoomLevel zoomLevel)
    {
        this.focus.position = transform.position;
        _camera.transform.LookAt(this.focus);
        CameraControls.Methods.FocusCamera(zoomLevel, this.focus, Camera.main);
    }

    public void SetCameraZoom(CameraControls.ZoomLevel zoomLevel)
    {
        CameraControls.Methods.FocusCamera(zoomLevel, this.focus, Camera.main);
    }

    /// <summary>
    /// Zoom out
    /// </summary>
    public void DecreaseCameraZoom()
    {
        float dist = Methods.DistanceToFocus(focus, _camera).magnitude;
        foreach (ZoomLevel zoomLevel in Enum.GetValues(typeof(ZoomLevel)))
        {
            if (dist + 10f < Constants.ZoomDistance[zoomLevel])
            {
                Methods.FocusCamera(zoomLevel, this.focus, _camera);
                return;
            }
        }
    }

    /// <summary>
    /// Zoom in
    /// </summary>
    public void IncreaseCameraZoom()
    {
        ZoomLevel? prev = null;
        float dist = Methods.DistanceToFocus(focus, _camera).magnitude;
        foreach (ZoomLevel zoomLevel in Enum.GetValues(typeof(ZoomLevel)))
        {
            if (dist - 10f> Constants.ZoomDistance[zoomLevel])
            {
                prev = zoomLevel;
                continue;
            }

            break;
        }
        Methods.FocusCamera(prev ?? ZoomLevel.System, focus, _camera);
    }

    public void TranslateCamera(Vector3 movement)
    {
        Methods.TranslateAround(movement, focus, _camera);   
    }

    public float DistanceToFocus()
    {
        return Methods.DistanceToFocus(focus, _camera).magnitude;
    }

    public void ToggleTacticalCamera()
    {
        // Manage state and handle cancelling in-transit camera movements 
        // See "StopCoroutine"
    }
    
    private void ActivateTacticalCamera()
    {
        // Define list of normal cameras
        // Define list of tactical cameras
        // Disable normal cameras
        // Activate tactical camera
        // Determine final location and rotation of tactical camera
        // Enable tactical UI camera
        // StartCoroutine - move tactical camera to zoomed out position and rotation
            // Consider how to cancel if deactivated while in transit
    }

    private void DeactivateTacticalCamera()
    {
        // Run something similar to ActivateTacticalCamera but backwards
    }
}