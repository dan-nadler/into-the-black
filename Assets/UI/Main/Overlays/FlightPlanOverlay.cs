using System.Collections.Generic;
using Generic.Flight;
using Ships;
using UI.Elements;
using UnityEngine;

namespace UI.Main.Overlays
{
    public class FlightPlanOverlay : Overlay
    {
        private GameObject _lineLocal;
        private GameObject _lineGlobal;
        
        private void Start()
        {
            _lineLocal = Resources.Load<GameObject>("FlightPlanLine Local");
            _lineGlobal = Resources.Load<GameObject>("FlightPlanLine Global");
        }
        
        protected override void OverlayIcons()
        {
            foreach (var o in FindObjectsOfType<TransportationShipFlightControl>())
            {
                var id = o.GetInstanceID();
                
                var flightPlanId = o.FlightPlan?.GetHashCode();

                // if there is a flightplan being rendered for this ship...
                if (Icons.ContainsKey(id))
                {
                    if (flightPlanId is null) // but there is no current flightplan, then delete the old one
                    {
                        Destroy(Icons[id]);
                    }
                    else if (Icons[id].GetHashCode() != flightPlanId) // and the flighplan is different, delete and draw the new one
                    {
                        Destroy(Icons[id]);
                        InstantiateLine(id, o);
                    }
                }
                else if (flightPlanId != null)
                {
                    InstantiateLine(id, o);
                }
            }
        }

        private void InstantiateLine(int id, TransportationShipFlightControl o)
        {
            switch (o.ShipType)
            {
                case ShipType.LOCAL_SUPPLY:
                    Icons[id] = Instantiate(_lineLocal, o.transform);
                    break;
                
                case ShipType.GLOBAL_SUPPLY:
                    Icons[id] = Instantiate(_lineGlobal, o.transform);
                    break;
            }
            
            (Transform, CargoAction)[] t = o.FlightPlan.OriginalPlan;
            Icons[id].GetComponent<Line>().points = new List<Vector3>();
            
            for (int i = 0; i < t.Length; i++)
            {
                Icons[id].GetComponent<Line>().points.Add(t[i].Item1.position);
            }
            Icons[id].GetComponent<Line>().CreatePoints();
        }
    }
}