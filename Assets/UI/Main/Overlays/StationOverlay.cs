using Stations;
using UnityEngine;

namespace UI.Main.Overlays
{
    public class StationOverlay : Overlay
    {
        private GameObject _icon;
        
        private void Start()
        {
            _icon = Resources.Load<GameObject>("StationIcon");
            MainCamera = Camera.main;
        }
        
        protected override void OverlayIcons()
        {
            foreach (var o in FindObjectsOfType<StationBehavior>())
            {
                var id = o.GetInstanceID();

                if (!Icons.ContainsKey(id)) Icons[id] = Instantiate(_icon, container);

                try
                {
                    Icons[id].transform.localPosition = ComputeIconLocationForScreenSpaceOverlay(o.transform);
                }
                catch (ObjectBehindCameraException)
                {
                    continue;
                }
            }
        }
    }
}