using Generic.Flight;
using Ships;
using UI.Elements;
using UnityEngine;

namespace UI.Main.Overlays
{
    public class ShipOverlay : Overlay
    {
        private GameObject _circleLocal;
        private GameObject _circleGlobal;

        private void Start()
        {
            MainCamera = Camera.main;
            _circleLocal = Resources.Load<GameObject>("Circle Local");
            _circleLocal.GetComponent<CircleOutline>().CirclePlane = CircleOutline.Planes.XY;
            _circleLocal.GetComponent<CircleOutline>().radius = 10;
            _circleLocal.GetComponent<LineRenderer>().startWidth = .5f;
            _circleLocal.GetComponent<LineRenderer>().endWidth = .5f;
            
            _circleGlobal = Resources.Load<GameObject>("Circle Global");
            _circleGlobal.GetComponent<CircleOutline>().CirclePlane = CircleOutline.Planes.XY;
            _circleGlobal.GetComponent<CircleOutline>().radius = 10;
            _circleGlobal.GetComponent<LineRenderer>().startWidth = .5f;
            _circleGlobal.GetComponent<LineRenderer>().endWidth = .5f;
            
        }

        private void ScaleIcons(GameObject icon)
        {
            LineRenderer lr = icon.GetComponent<LineRenderer>();
            CircleOutline co = icon.GetComponent<CircleOutline>();

            float distance = Vector3.Distance(MainCamera.transform.position, icon.transform.position);
            float scaleFactor = distance / 200f;

            lr.startWidth = .2f * scaleFactor;
            lr.endWidth = .2f * scaleFactor;
            
            co.radius = 10 * scaleFactor;
        }
        
        protected override void OverlayIcons()
        {
            foreach (var o in FindObjectsOfType<FlightControl>())
            {
                var id = o.GetInstanceID();

                if (!Icons.ContainsKey(id))
                {
                    switch (o.ShipType)
                    {
                        case ShipType.LOCAL_SUPPLY:
                            Icons[id] = Instantiate(_circleLocal, o.transform);
                            break;
                        
                        case ShipType.GLOBAL_SUPPLY:
                            Icons[id] = Instantiate(_circleGlobal, o.transform);
                            break;
                        
                        default:
                            Icons[id] = Instantiate(_circleLocal, o.transform);
                            break;
                    }
                }
                Icons[id].transform.localPosition = Vector3.zero;
                Icons[id].transform.LookAt(MainCamera.transform);
            }
        }
    }
}