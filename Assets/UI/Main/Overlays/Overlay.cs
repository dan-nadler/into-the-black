using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.Main.Overlays
{
    public class Overlay : MonoBehaviour, IPointerClickHandler
    {
        public Transform container;
        protected UnityEngine.Camera MainCamera;
        private bool _showIcons = false;
        protected readonly Dictionary<int, GameObject> Icons = new Dictionary<int, GameObject>();

        private void Update()
        {
            if (_showIcons) OverlayIcons();
        }

        protected virtual void OverlayIcons()
        {
            throw new NotImplementedException();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            _showIcons = !_showIcons;
            if (!_showIcons) ClearIcons();
        }

        protected class ObjectBehindCameraException : Exception
        {
        };

        protected Vector2 ComputeIconLocationForScreenSpaceOverlay(Transform obj)
        {
            Vector3 viewportPosition = MainCamera.WorldToViewportPoint(obj.position);
            if (viewportPosition.z < 0) throw new ObjectBehindCameraException();
            float w = container.GetComponent<RectTransform>().rect.width;
            float h = container.GetComponent<RectTransform>().rect.height;
            var pos = new Vector2(
                viewportPosition.x * w - w / 2,
                viewportPosition.y * h - h / 2
            );
            return pos;
        }

        protected Vector2 ComputeIconLocationForScreenSpaceCamera(Transform obj)
        {
            throw new NotImplementedException();
        }

        protected virtual void ClearIcons()
        {
            foreach (var icon in Icons.Values) Destroy(icon);

            Icons.Clear();
        }
    }
}