namespace UI.Main.Overlays
{
    public class LogisticsZoneOverlay : Overlay
    {
        protected override void OverlayIcons()
        {
            RegionOutline[] ros = FindObjectsOfType<RegionOutline>();
            foreach (RegionOutline ro in ros)
            {
                ro.Visible = true;
            }
        }

        protected override void ClearIcons()
        {
            RegionOutline[] ros = FindObjectsOfType<RegionOutline>();
            foreach (RegionOutline ro in ros)
            {
                ro.Visible = false;
            }
        }
    }
}