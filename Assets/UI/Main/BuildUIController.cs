using System.Collections.Generic;
using UI.Elements;
using UnityEngine;

namespace UI.Main
{
    public class BuildUIController : MonoBehaviour
    {
        public CircleOutline buildRadius;
        public Line xzLine;
        public Line yLine;
        public GlobalUIState state;
 
        private void Update()
        {
            if (state.buildState != GlobalUIState.BuildState.OFF && state.BuildPosition != null)
            {
                buildRadius.gameObject.SetActive(true);
                xzLine.gameObject.SetActive(true);
                yLine.gameObject.SetActive(true);
                
                // Update Cursor Highlight

                Vector3 buildPosition = (Vector3) state.BuildPosition;
                Vector3 projectOnPlane = Vector3.ProjectOnPlane(buildPosition, Vector3.up);
                
                // Update Radius
                Vector3 position = transform.position;
                buildRadius.radius = (projectOnPlane - position).magnitude;
                
                // Update XZ Line    
                xzLine.points = new List<Vector3>()
                {
                    position,
                    projectOnPlane
                };
        
                // Update Y Line
                yLine.points = new List<Vector3>()
                {
                    projectOnPlane,
                    buildPosition
                };
            }
            else
            {
                buildRadius.gameObject.SetActive(false);
                xzLine.gameObject.SetActive(false);
                yLine.gameObject.SetActive(false);
            }
        }
    }
}
