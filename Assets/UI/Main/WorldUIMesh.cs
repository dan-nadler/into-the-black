using UI.Main;
using UnityEngine;

namespace UI.Elements
{
    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(MeshFilter))]
    public class WorldUIMesh : MonoBehaviour
    {
        public float radius = 3000f;
        public float width = 0.005f;
        public GlobalUIState state;

        private void Update()
        {
            if (state.buildState != GlobalUIState.BuildState.OFF)
            {
                if (!gameObject.activeSelf) gameObject.SetActive(true);
                GetComponent<MeshFilter>().mesh = CreateMesh();
            }
            else if (state.buildState == GlobalUIState.BuildState.OFF && gameObject.activeSelf)
            {
                gameObject.SetActive(false);
            }
        }

        private Mesh CreateMesh()
        {
            Collider[] stations = 
                Physics.OverlapSphere(transform.position, radius, UnityConstants.Layers.StationsLayer);
            Vector3[] verts = new Vector3[stations.Length * 8];
            int[] tris = new int[stations.Length * 12];
            Vector3[] norms = new Vector3[stations.Length * 8];
            Vector2[] uvs = new Vector2[stations.Length * 8];
            for (int i = 0; i < stations.Length; i++)
            {
                int vi = i * 8;
                // Quad 1
                Vector3 normal = Vector3.up;
                verts[vi] = transform.InverseTransformPoint(stations[i].transform.position); // UL
                Vector3 projectOnPlane = Vector3.ProjectOnPlane(verts[vi], normal);
                verts[vi + 1] = projectOnPlane; // BL
                verts[vi + 2] = projectOnPlane + new Vector3(width, 0, 0); // BR
                verts[vi + 3] = verts[vi] + new Vector3(width, 0, 0); // UR
                
                // Quad 2
                Vector3 quad2Transform = new Vector3(width/2, 0, -width/2);
                verts[vi + 4] = verts[vi] + quad2Transform;
                verts[vi + 5] = verts[vi + 1] + quad2Transform;
                verts[vi + 6] = verts[vi + 1] + new Vector3(0, 0, width) + quad2Transform;
                verts[vi + 7] = verts[vi] + new Vector3(0, 0, width) + quad2Transform;

                int ti = i * 12;
                // tri 1
                tris[ti] = vi + 1; // 1
                tris[ti + 1] = vi; // 0
                tris[ti + 2] = vi + 2; // 2
                // tri 2
                tris[ti + 3] = vi; // 0
                tris[ti + 4] = vi + 3; // 3
                tris[ti + 5] = vi + 2; // 2
                
                // tri 3
                tris[ti + 6] = vi + 5;
                tris[ti + 7] = vi + 4;
                tris[ti + 8] = vi + 6;
                // tri 4
                tris[ti + 9] = vi + 4;
                tris[ti + 10] = vi + 7;
                tris[ti + 11] = vi + 6;

                norms[vi] = -Vector3.forward;
                norms[vi + 1] = -Vector3.forward;
                norms[vi + 2] = -Vector3.forward;
                norms[vi + 3] = -Vector3.forward;
                
                norms[vi + 4] = -Vector3.forward;
                norms[vi + 5] = -Vector3.forward;
                norms[vi + 6] = -Vector3.forward;
                norms[vi + 7] = -Vector3.forward;

                uvs[vi] = new Vector2(0, 0);
                uvs[vi + 1] = new Vector2(1, 0);
                uvs[vi + 2] = new Vector2(0, 1);
                uvs[vi + 3] = new Vector2(1, 1);
                
                uvs[vi + 4] = new Vector2(0, 0);
                uvs[vi + 5] = new Vector2(1, 0);
                uvs[vi + 6] = new Vector2(0, 1);
                uvs[vi + 7] = new Vector2(1, 1);
            }

            Mesh mesh = new Mesh();
            mesh.vertices = verts;
            mesh.triangles = tris;
            mesh.normals = norms;
            mesh.uv = uvs;

            return mesh;
        }
    }
}