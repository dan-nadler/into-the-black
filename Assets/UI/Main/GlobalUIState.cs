using Stations;
using UnityEngine;

namespace UI.Main
{
    public class GlobalUIState : MonoBehaviour
    {
        public enum BuildState
        {
            OFF,
            XZ,
            Y
        }

        public Vector3? BuildPosition;
        public BuildState buildState = BuildState.OFF;

        public StationBehavior[] selectedStations;

        public enum ViewType
        {
            STANDARD,
            TACTICAL
        }

        public ViewType viewType = ViewType.STANDARD;
    }
}