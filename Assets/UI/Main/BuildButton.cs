using Frames;
using Generic;
using StationConstants;
using Stations;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

namespace UI.Main
{
    public class BuildButton : MonoBehaviour, IPointerClickHandler
    {
        public KeyCode keyCode;

        // Station Building
        public GlobalUIState globalUIState;
        public GameObject buildStationPrefab;
        public GameObject stationPrefab;
        public StationType stationType;

        private GameObject _activeBuildObject;
        private bool _buildActive;

        private FrameController _frameController;
        
        private void Start()
        {
            _frameController = GameObject.Find("FrameController").GetComponent<FrameController>();
            _activeBuildObject = Instantiate(buildStationPrefab, Vector3.zero, Quaternion.identity);
            _activeBuildObject.SetActive(false);
        }

        private void Update()
        {
            if (_buildActive)
            {
                ManageStationBuildState();
            }
            else
            {
                if (Input.GetKeyDown(keyCode) && gameObject.activeSelf)
                {
                    StartBuild();
                }
            }
        }

        private void ManageStationBuildState()
        {
            if (globalUIState.buildState != GlobalUIState.BuildState.OFF)
            {
                this.DrawBuildStationIcon();

                if (Input.GetMouseButtonDown((int) MouseButton.RightMouse) || Input.GetKeyDown(KeyCode.Escape))
                {
                    CleanUp();
                }
            }

            if (globalUIState.buildState == GlobalUIState.BuildState.XZ)
            {
                if (Input.GetMouseButtonDown((int) MouseButton.LeftMouse))
                {
                    globalUIState.buildState = GlobalUIState.BuildState.Y;
                }
            }

            else if (globalUIState.buildState == GlobalUIState.BuildState.Y)
            {
                if (Input.GetMouseButtonDown((int) MouseButton.LeftMouse))
                {
                    // Instantiate the new station object and set the requisite parameters
                    GameObject newStation = Instantiate(this.stationPrefab, this._activeBuildObject.transform.position,
                        this._activeBuildObject.transform.rotation, _frameController.activeFrame.transform);
                    StationBehavior sb = newStation.GetComponent<StationBehavior>();
                    sb.underConstruction = true;
                    sb.stationType = stationType;
                    CleanUp();
                }
            }
        }

        private void CleanUp()
        {
            globalUIState.buildState = GlobalUIState.BuildState.OFF;
            globalUIState.BuildPosition = null;
            this._activeBuildObject.SetActive(false);
            _buildActive = false;
        }

        private void DrawBuildStationIcon()
        {
            if (globalUIState.buildState == GlobalUIState.BuildState.XZ)
            {
                try
                {
                    // TODO use focus position for y coordinate of the normal vector
                    var position = Utilities.CursorPositionInWorld(Camera.main, Vector3.up, Vector3.zero);
                    _activeBuildObject.transform.position = position;
                    _activeBuildObject.SetActive(true);
                    globalUIState.BuildPosition = position;
                }
                catch (Utilities.RaycastNotHitException)
                {
                    // ignored
                }
            }

            if (globalUIState.buildState == GlobalUIState.BuildState.Y)
            {
                // We use the ray direction to compute the normal vector, so can't use Utilities.CursorPositionInWorld
                // here. Otherwise, this is the same.
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Vector3 d = new Vector3(ray.direction.x, 0, ray.direction.z).normalized;
                Plane yPlane = new Plane(d, _activeBuildObject.transform.position);
                float distance = 0;
                if (yPlane.Raycast(ray, out distance))
                {
                    var position = _activeBuildObject.transform.position;
                    position.y = ray.GetPoint(distance).y;
                    _activeBuildObject.transform.position = position;
                    globalUIState.BuildPosition = position;
                }
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            StartBuild();
        }

        private void StartBuild()
        {
            if (globalUIState.buildState == GlobalUIState.BuildState.OFF)
            {
                _buildActive = true;
                globalUIState.buildState = GlobalUIState.BuildState.XZ;
            }
        }
    }
}