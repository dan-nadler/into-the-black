using System;
using System.Collections;
using System.Linq;
using PlasticGui;
using UI.Main.Selection;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.Main
{
    public class GeneralUICommands : MonoBehaviour, IScrollHandler
    {
        private CameraControl _mainCameraControl;
        private Transform _mainCamera;
        public Transform tacticalCamera;
        public AnimationCurve scrollSensitivityCurve;
        public float minScrollSensitivityDistance;
        public float maxScrollSensitivityDistance;
        public SelectedDictionary selectedDictionary;
        public GlobalUIState state;
        
        public float cameraTransitionDuration = 0.7f;
        private float previousCameraDistanceToFocus;
        private Coroutine _transition;

        private void Start()
        {
            _mainCamera = Camera.main.transform;
            _mainCameraControl = Camera.main.GetComponent<CameraControl>();
        }

        public void OnScroll(PointerEventData eventData)
        {
            float relativeDistance =
            (
                Mathf.Max(_mainCameraControl.DistanceToFocus(), minScrollSensitivityDistance)
                - minScrollSensitivityDistance
            ) / maxScrollSensitivityDistance;
            float curvedMovement = scrollSensitivityCurve.Evaluate(relativeDistance) * eventData.scrollDelta.y;

            _mainCameraControl.TranslateCamera(new Vector3(0, 0, curvedMovement));
        }

        private void OnGUI()
        {
            if (Event.current.Equals(Event.KeyboardEvent("F")))
            {
                _mainCameraControl.SetCameraFocus(selectedDictionary.SelectedTable.Values.ToArray());
            }
            
            if (Event.current.Equals(Event.KeyboardEvent("space")))
            {
                Debug.Log("Toggle Camera");
                
                if (_transition != null) StopCoroutine(_transition);
                switch (state.viewType)
                {
                    case GlobalUIState.ViewType.STANDARD:
                        state.viewType = GlobalUIState.ViewType.TACTICAL;
                        previousCameraDistanceToFocus = _mainCameraControl.DistanceToFocus();
                        _transition = StartCoroutine(TransitionViewTypeCamera(GlobalUIState.ViewType.TACTICAL, Time.time + cameraTransitionDuration));
                        break;
                    
                    case GlobalUIState.ViewType.TACTICAL:
                        _transition = StartCoroutine(TransitionViewTypeCamera(GlobalUIState.ViewType.STANDARD, Time.time + cameraTransitionDuration));
                        state.viewType = GlobalUIState.ViewType.STANDARD;
                        break;
                };
            }
        }

        private IEnumerator TransitionViewTypeCamera(GlobalUIState.ViewType toViewType, float transitionEndTime)
        {
            float start = Time.time;
            Vector3 initialPosition = _mainCamera.position;
            
            tacticalCamera.localPosition = Vector3.zero;
            tacticalCamera.rotation = Quaternion.identity;
            Vector3 finalPosition;
            
            if (toViewType == GlobalUIState.ViewType.TACTICAL)
            {
                finalPosition = new Vector3(1000f, 2500f, 1000f) + _mainCameraControl.focus.position;
            }
            else
            {
                finalPosition = _mainCameraControl.focus.position - 
                                _mainCamera.forward.normalized * previousCameraDistanceToFocus;
            }

            float t = 0;
            while (Time.time <= transitionEndTime)
            {
                _mainCamera.position = Vector3.Lerp(initialPosition, finalPosition, t);
                _mainCamera.LookAt(_mainCameraControl.focus.position);
                t += Time.deltaTime / (transitionEndTime - start);
                yield return null;
            }

            Debug.Log("Tactical Camera Finished");
        }
        
    }
}
