using System;
using System.Collections.Generic;
using UnityEngine;

namespace UI.Main.Selection
{
    public class SelectedDictionary : MonoBehaviour
    {
        public Dictionary<int, GameObject> SelectedTable = new Dictionary<int, GameObject>();

        public HashSet<Action> onAddSelectedListeners = new HashSet<Action>();
        public HashSet<Action> onAddSelectedChange = new HashSet<Action>();

        public void addSelected(GameObject go)
        {
            int id = go.GetInstanceID();

            if (!(SelectedTable.ContainsKey(id)))
            {
                SelectedTable.Add(id, go);
                go.AddComponent<SelectionComponent>();
                Debug.Log("Added " + id + " to selected dict");
            }

            foreach (Action a in onAddSelectedListeners)
                a();
            
            foreach (Action a in onAddSelectedChange)
                a();
        }

        public void deselect(int id)
        {
            Destroy(SelectedTable[id].GetComponent<SelectionComponent>());
            SelectedTable.Remove(id);
            
            foreach (Action a in onAddSelectedChange)
                a();
        }

        public void deselectAll()
        {
            foreach(KeyValuePair<int,GameObject> pair in SelectedTable)
            {
                if(pair.Value != null)
                {
                    Destroy(SelectedTable[pair.Key].GetComponent<SelectionComponent>());
                }
            }
            SelectedTable.Clear();
            
            foreach (Action a in onAddSelectedChange)
                a();
        }
    }
}