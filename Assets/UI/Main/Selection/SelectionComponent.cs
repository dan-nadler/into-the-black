using System;
using UnityEngine;

namespace UI.Main.Selection
{
    public class SelectionComponent : MonoBehaviour
    {
        public GameObject stationSelectionPrefab;
        private GameObject _instantiatedSelectionPrefab;

        private void Awake()
        {
            stationSelectionPrefab = Resources.Load<GameObject>("Station Square");
        }

        private void Start()
        {
            _instantiatedSelectionPrefab = Instantiate(stationSelectionPrefab, transform);
        }

        private void OnDestroy()
        {
            Destroy(_instantiatedSelectionPrefab);
        }
    }
}