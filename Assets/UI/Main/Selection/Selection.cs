using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI.Main.Selection
{
    [RequireComponent(typeof(Image))] // to make the object raycast-able
    public class Selection : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        private Camera _mainCamera;
        private CameraControl _cameraControl;
        public SelectedDictionary selectedTable;

        public MeshCollider selectionBox;
        public Mesh selectionMesh;
        public GameObject selectionGameObject;
        
        private Vector2 _p1;
        private Vector2 _p2;
        
        private float _lastClick;
        private bool _heldDown = false;
        private float _doubleClickDelay = 0.5f;
        private float _dragThreshold = 20;

        private void Start()
        {
            _mainCamera = Camera.main;
            _cameraControl = _mainCamera.GetComponent<CameraControl>();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _heldDown = true;
            
            Debug.Log("Selection Down Detected");
            if (Time.time - _lastClick < _doubleClickDelay)
            {
                Debug.Log("Double click detected.");
                Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
                
                Debug.DrawRay(ray.origin, ray.direction * 100f, Color.magenta, 3f);
                RayCastForStations(ray, out Transform stationTransform);
                
                if (stationTransform)
                    _cameraControl.SetCameraFocus(stationTransform);
            }
            else
            {
                _lastClick = Time.time;
                _p1 = Input.mousePosition;
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _heldDown = false;
            
            _p2 = Input.mousePosition;
            if (Vector2.Distance(_p1, _p2) > _dragThreshold)
                MakeSelectionMesh();
        }
        
        private void RayCastForStations(Ray ray, out Transform stationTransform)
        {
            stationTransform = null;
            if (Physics.Raycast(ray, out RaycastHit _hit, 50000.0f, UnityConstants.Layers.StationsLayer))
            {
                stationTransform = _hit.transform;
            }
        }

        private void MakeSelectionMesh()
        {
            Vector3[] _verts = new Vector3[4];
            Vector3[] _vecs = new Vector3[4];
            int i = 0;
            _p2 = Input.mousePosition;
            Vector2[] _corners = getBoundingBox(_p1, _p2);
            
            foreach (Vector2 corner in _corners)
            {
                Ray ray = _mainCamera.ScreenPointToRay(corner);

                _verts[i] = ray.origin + (ray.direction * 50000f); // furthest corners
                _vecs[i] = ray.origin - _verts[i]; // vectors to the corners
                Debug.DrawLine(ray.origin, _verts[i], Color.red, 1.0f);
                Debug.DrawLine(ray.origin, _verts[i], Color.blue, 1.0f);

                i++;
            }

            //generate the mesh
            selectionMesh = generateSelectionMesh(_verts, _vecs);
            selectionBox = selectionGameObject.AddComponent<MeshCollider>();
            selectionBox.sharedMesh = selectionMesh;
            selectionBox.convex = true;
            selectionBox.isTrigger = true;

            if (!Input.GetKey(KeyCode.LeftShift))
            {
                selectedTable.deselectAll();
            }

            Destroy(selectionBox, 0.02f);
        }
        
        Vector2[] getBoundingBox(Vector2 p1, Vector2 p2)
        {
            Vector2 newP1;
            Vector2 newP2;
            Vector2 newP3;
            Vector2 newP4;

            if (p1.x < p2.x) //if p1 is to the left of p2
            {
                if (p1.y > p2.y) // if p1 is above p2
                {
                    newP1 = p1;
                    newP2 = new Vector2(p2.x, p1.y);
                    newP3 = new Vector2(p1.x, p2.y);
                    newP4 = p2;
                }
                else //if p1 is below p2
                {
                    newP1 = new Vector2(p1.x, p2.y);
                    newP2 = p2;
                    newP3 = p1;
                    newP4 = new Vector2(p2.x, p1.y);
                }
            }
            else //if p1 is to the right of p2
            {
                if (p1.y > p2.y) // if p1 is above p2
                {
                    newP1 = new Vector2(p2.x, p1.y);
                    newP2 = p1;
                    newP3 = p2;
                    newP4 = new Vector2(p1.x, p2.y);
                }
                else //if p1 is below p2
                {
                    newP1 = p2;
                    newP2 = new Vector2(p1.x, p2.y);
                    newP3 = new Vector2(p2.x, p1.y);
                    newP4 = p1;
                }
            }

            Vector2[] corners = {newP1, newP2, newP3, newP4};
            return corners;
        }

        //generate a mesh from the 4 bottom points
        Mesh generateSelectionMesh(Vector3[] corners, Vector3[] vecs)
        {
            Vector3[] verts = new Vector3[8];
            int[] tris =
            {
                0, 1, 2, 2, 1, 3, 4, 6, 0, 0, 6, 2, 6, 7, 2, 2, 7, 3, 7, 5, 3, 3, 5, 1, 5, 0, 1, 1, 4, 0, 4, 5, 6, 6, 5,
                7
            }; //map the tris of our cube

            // furthest corenrs
            for (int i = 0; i < 4; i++)
            {
                verts[i] = corners[i] ;
                Debug.DrawRay(verts[i], verts[i].normalized * 100, Color.yellow, 10f);
            }

            // near corners - in this case, these should all be the camera position in local space. right?
            for (int j = 4; j < 8; j++)
            {
                verts[j] = corners[j - 4] + vecs[j - 4];
                Debug.DrawRay(verts[j], corners[j-4] - verts[j], Color.magenta, 10f);
            }

            Mesh selectionMesh = new Mesh();
            selectionMesh.vertices = verts;
            selectionMesh.triangles = tris;
            
            return selectionMesh;
        }

        private void OnGUI()
        {
            if (_heldDown && (Vector2.Distance(_p1, Input.mousePosition) > _dragThreshold))
            {
                var rect = ScreenRectUtils.GetScreenRect(_p1, Input.mousePosition);
                ScreenRectUtils.DrawScreenRect(rect, new Color(0.8f, 0.8f, 0.95f, 0.25f));
                ScreenRectUtils.DrawScreenRectBorder(rect, 2, new Color(0.8f, 0.8f, 0.95f));
            }
        }
    }
}