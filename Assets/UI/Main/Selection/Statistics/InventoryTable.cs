using System;
using EconomicsConstants;
using Generic.Economics;
using TMPro;
using UnityEngine;

namespace UI.Main.Selection.Statistics
{
    public class InventoryTable : MonoBehaviour
    {
        public GameObject row;
        private void Start()
        {
            RectTransform rt = GetComponent<RectTransform>();
            float h = Enum.GetValues(typeof(AssetType)).Length * 30;
            float w = rt.rect.width;
            rt.sizeDelta = new Vector2(w, h);
            rt.anchoredPosition = Vector2.zero;
            
            foreach (AssetType asset in Enum.GetValues(typeof(AssetType)))
            {
                GameObject tableRow = Instantiate(row, transform);
                tableRow.transform.Find("Resource").GetComponent<TextMeshProUGUI>().text = asset.ToString();
                foreach (AggregateInventoryStats a in tableRow.transform.GetComponentsInChildren<AggregateInventoryStats>())
                {
                    a.assetType = asset;
                }    
            }
        }
    }
}