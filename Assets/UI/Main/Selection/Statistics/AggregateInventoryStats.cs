using System.Collections.Generic;
using System.Globalization;
using EconomicsConstants;
using Generic.Economics;
using StationConstants;
using Stations;
using TMPro;
using UnityEngine;

namespace UI.Main.Selection.Statistics
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class AggregateInventoryStats : MonoBehaviour
    {
        public enum StatisticType
        {
            SUM_INVENTORY,
            SUM_PRODUCTION,
            SUM_CONSUMPTION,
            NET_CONSUMPTION_PRODUCTION,
            SUM_LOCAL_AVAILABLE,
            SUM_GLOBAL_AVAILABLE,
        };

        public AssetType assetType;
        public StatisticType statistic;
        public int decimals = 2;

        private SelectedDictionary _selectedTable;
        private TextMeshProUGUI _textField;
        private NumberFormatInfo _nfi;

        private void Start()
        {
            _textField = GetComponent<TextMeshProUGUI>();
            _selectedTable = GameObject.FindWithTag("SelectionController").GetComponent<SelectedDictionary>();
            _nfi = new CultureInfo( "en-US", false ).NumberFormat;
            _nfi.NumberDecimalDigits = decimals;
        }

        public void Update()
        {
            _textField.text = ComputeStatistic(statistic).ToString("N", _nfi);
        }

        private float ComputeStatistic(StatisticType stat)
        {
            List<Asset> assets = GetSelectedAssets(assetType);
            float sum;
            List<StationBehavior> stations;
            switch (stat)
            {
                case StatisticType.SUM_INVENTORY:
                    sum = 0f;
                    foreach (Asset asset in assets) sum += asset.Quantity;
                    return sum;
                
                case StatisticType.SUM_PRODUCTION:
                    stations = GetSelectedStations(null);
                    sum = 0f;
                    foreach (StationBehavior s in stations)
                    {
                        if (s.Behavior.Produces.ContainsKey(assetType)) sum += s.Behavior.Produces[assetType];
                    }
                    return sum;
                
                case StatisticType.SUM_CONSUMPTION:
                    stations = GetSelectedStations(null);
                    sum = 0f;
                    foreach (StationBehavior s in stations)
                    {
                        if (s.Behavior.Consumes.ContainsKey(assetType)) sum += s.Behavior.Consumes[assetType];
                    }
                    return sum;
                
                case StatisticType.NET_CONSUMPTION_PRODUCTION:
                    return ComputeStatistic(StatisticType.SUM_PRODUCTION) -
                           ComputeStatistic(StatisticType.SUM_CONSUMPTION);
                
                case StatisticType.SUM_LOCAL_AVAILABLE:
                    stations = GetSelectedStations(null);
                    sum = 0f;
                    foreach (StationBehavior s in stations)
                    {
                        float q = 0f;
                        
                        // the total amount in inventory
                        if (s.Inventory.Holdings.ContainsKey(assetType)) q += s.Inventory.Holdings[assetType].Quantity;
                        // minus the amount reserved for station use
                        if (s.Behavior.StationReserves.ContainsKey(assetType)) q -= s.Behavior.StationReserves[assetType];
                        // and no more than the amount reserved for local use
                        if (s.stationType is StationType.LOGISTICS && s.Behavior.LocalReserves.ContainsKey(assetType))
                            q = Mathf.Min(q, s.Behavior.LocalReserves[assetType]);
                        
                        if (q > 0) sum += q;
                    }

                    return sum;
                    
                case StatisticType.SUM_GLOBAL_AVAILABLE:
                    stations = GetSelectedStations(StationType.LOGISTICS);
                    sum = 0f;
                    foreach (StationBehavior s in stations)
                    {
                        float q = 0f;
                        
                        // the total amount in inventory
                        if (s.Inventory.Holdings.ContainsKey(assetType)) q += s.Inventory.Holdings[assetType].Quantity;
                        // minus the amount reserved for station use
                        if (s.Behavior.StationReserves.ContainsKey(assetType)) q -= s.Behavior.StationReserves[assetType];
                        // minus the amount reserved for local use
                        if (s.Behavior.LocalReserves.ContainsKey(assetType)) q -= s.Behavior.LocalReserves[assetType];
                        
                        if (q > 0) sum += q;
                    }

                    return sum;

                default:
                    return 0f;
            }
        }

        private List<StationBehavior> GetSelectedStations(StationType? stationType)
        {
            List<StationBehavior> list = new List<StationBehavior>();

            foreach (KeyValuePair<int, GameObject> pair in _selectedTable.SelectedTable)
            {
                if (pair.Value.TryGetComponent(out StationBehavior c))
                {
                    if (stationType == null)
                    {
                        list.Add(c);
                    }
                    else if (stationType == c.stationType)
                    {
                        list.Add(c);
                    }
                }
            }

            return list;
        }
        
        private List<Asset> GetSelectedAssets(AssetType at)
        {
            List<Asset> list = new List<Asset>();

            foreach (KeyValuePair<int, GameObject> pair in _selectedTable.SelectedTable)
            {
                if (pair.Value.TryGetComponent(out StationBehavior c))
                {
                    Dictionary<AssetType, Asset> holdings = c.Inventory.Holdings;
                    if (holdings.ContainsKey(at)) list.Add(holdings[at]);                    
                }
            }

            return list;
        }
    }
}