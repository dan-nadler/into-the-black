using System.Collections.Generic;
using EconomicsConstants;
using Stations;
using StationTypes;
using TMPro;
using UI.Main.Selection;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Button = UnityEngine.UI.Button;

namespace UI.Main
{
    [RequireComponent(typeof(CanvasGroup))]
    public class ConstructionButtons : MonoBehaviour, IPointerClickHandler
    {
        public SelectedDictionary selectedDictionary;
        public GameObject buttonPrefab;

        public GameObject[] receivingPlatformShips;
        public GameObject[] tier1ShipyardShips;

        private void Start()
        {
            selectedDictionary.onAddSelectedChange.Add(onAddSelectedCallback);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
        }

        public GameObject buttonPanel(Transform parent)
        {
            GameObject panel = new GameObject();
            panel.transform.parent = parent;
            RectTransform rt = panel.AddComponent<RectTransform>();
            rt.localPosition = Vector3.zero;
            rt.localRotation = Quaternion.identity;
            rt.localScale = Vector3.one;
            
            GridLayoutGroup glg = panel.AddComponent<GridLayoutGroup>();
            glg.padding = new RectOffset(10, 10, 10, 10);
            glg.cellSize = new Vector2(50, 50);
            glg.spacing = new Vector2(10, 10);
            
            return panel;
        }

        public void buildShip(GameObject shipToBuild, GameObject station)
        {
            Instantiate(
                shipToBuild, 
                station.transform.localPosition + new Vector3(10f, 0f, 0f), 
                Quaternion.identity,
                station.transform.parent.transform
            );
        }

        public void onAddSelectedCallback()
        {
            for (int i = 0; i < transform.childCount; i++)
                Destroy(transform.GetChild(i).gameObject);

            GameObject shipyard = null;
            GameObject receivingPlatform = null;
            
            HashSet<Behavior> stationTypes = new HashSet<Behavior>();
            foreach (GameObject go in selectedDictionary.SelectedTable.Values)
            {
                Behavior behavior = go.GetComponent<StationBehavior>().Behavior;
                if (behavior is Shipyard && shipyard is null) shipyard = go.gameObject;
                if (behavior is ReceivingPlatform && receivingPlatform is null) receivingPlatform = go.gameObject;
                
                stationTypes.Add(behavior);
            }

            if (shipyard != null) // Tier 1 Shipyard
            {
                // Ship Construction Options
                GameObject shipyardPanel = buttonPanel(transform);
                foreach (GameObject g in tier1ShipyardShips)
                {
                    var ship = Instantiate(buttonPrefab, shipyardPanel.transform);
                    ship.GetComponent<Button>().onClick.AddListener(() =>
                    {
                        Debug.Log($"Construct {g.name}.");
                        buildShip(g, shipyard);
                    });
                    ship.GetComponentInChildren<TMP_Text>().text = $"{g.name[0]}";    
                }
            }

            if (receivingPlatform != null) // Receiving Platform
            {
                // Asset Delivery Options
                GameObject rpPanel = buttonPanel(transform);
                AssetType[] assets = 
                    {AssetType.STEEL, AssetType.HULL, AssetType.FUEL, AssetType.ENGINE, AssetType.SUPPLIES};
                
                foreach (AssetType a in assets)
                {
                    var assetButton = Instantiate(buttonPrefab, rpPanel.transform);
                    assetButton.GetComponent<Button>().onClick
                        .AddListener(() =>
                        {
                            Debug.Log("Sending " + a + " to receiving platform.");
                            // TODO arrange a delivery to the receiving platform -- maybe just a timer?
                            receivingPlatform.GetComponent<StationBehavior>().Inventory.Holdings[a].Add(
                                EconomicsConstants.Fields.LotSizes[a] * 5f // 5 lots per delivery
                            );
                        });
                    assetButton.GetComponentInChildren<TMP_Text>().text = a.ToString()[0].ToString();
                }
                
                // Ship Delivery Options
                GameObject rpShipPanel = buttonPanel(transform);
                foreach (GameObject g in receivingPlatformShips)
                {
                    var ship = Instantiate(buttonPrefab, rpShipPanel.transform);
                    ship.GetComponent<Button>().onClick.AddListener(() =>
                    {
                        Debug.Log($"Construct {g.name}.");
                        buildShip(g, receivingPlatform);
                    });
                    ship.GetComponentInChildren<TMP_Text>().text = $"{g.name[0]}";    
                }
            }
        }
    }
}