using System;
using System.Collections.Generic;
using System.Linq;
using Stations;
using TMPro;
using UI.Main.Selection;
using UnityEngine;

namespace UI.Main.StationInfoPanel
{
    public class StationInfoPanel : MonoBehaviour
    {
        public TMP_InputField stationName;
        public TMP_Dropdown stationVariantsDropdown;
        public SelectedDictionary selectedDictionary;

        private void Start()
        {
            selectedDictionary.onAddSelectedListeners.Add(onStationSelected);
        }

        public void onStationSelected()
        {
            Debug.Log("Station selected callback"); // TODO deal with one or multiple selected stations
            if (selectedDictionary.SelectedTable.Count == 1)
            {
                UpdateStationObjects();
            }
            else
            {
                // Switch display to multi-station or hide?
            }
        }

        private void UpdateStationObjects()
        {
            GameObject station = selectedDictionary.SelectedTable.Values.First();
            stationName.text = station.name;

            Behavior behavior = station.GetComponent<StationBehavior>().Behavior;
            if (behavior.subType != null)
            {
                stationVariantsDropdown.ClearOptions();
                stationVariantsDropdown.AddOptions(behavior.SubTypes);
                stationVariantsDropdown.SetValueWithoutNotify(behavior.SubTypes.IndexOf(behavior.subType));
                stationVariantsDropdown.onValueChanged.AddListener(
                    delegate { DropdownValueChanged(stationVariantsDropdown, behavior); }
                );
            }
            else
            {
                stationVariantsDropdown.ClearOptions();
            }
        }
        
        void DropdownValueChanged(TMP_Dropdown change, Behavior behavior)
        {
            behavior.subType = behavior.SubTypes[change.value];
        }
    }
}
