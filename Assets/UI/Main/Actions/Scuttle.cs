using System;
using System.Collections.Generic;
using Stations;
using UI.Main.Selection;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.Main.Actions
{
    public class Scuttle : MonoBehaviour, IPointerClickHandler
    {
        private SelectedDictionary _selectedTable;

        private void Start()
        {
            _selectedTable = GameObject.FindWithTag("SelectionController").GetComponent<SelectedDictionary>();
        }


        public void OnPointerClick(PointerEventData eventData)
        {
            foreach (KeyValuePair<int, GameObject> pair in _selectedTable.SelectedTable)
            {
                if (pair.Value.TryGetComponent<StationBehavior>(out StationBehavior sb))
                {
                    sb.ToggleScuttle();
                }   
            }
        }
    }
}