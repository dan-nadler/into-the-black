using System;
using System.Collections.Generic;
using System.Linq;
using EconomicsConstants;
using Frames;
using Generic.Economics;
using JetBrains.Annotations;
using StationConstants;
using Stations;
using StationTypes;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace StationTypes
{
    public class UnderConstruction : Behavior
    {
        public StationConstants.StationType StationTypeUnderConstruction;
        public bool hasConstructor = false;
        
        public UnderConstruction(StationConstants.StationType stationType)
        {
            this.StationTypeUnderConstruction = stationType;

            switch (stationType)
            {
                default:
                    //Consumes[AssetType.HULL] = 100;
                    Consumes[AssetType.STEEL] = EconomicsConstants.Fields.LotSizes[AssetType.STEEL] * 1f;
                    Produces[AssetType.STATION] = EconomicsConstants.Fields.LotSizes[AssetType.STATION] * 1f;
                    break;
            }
        }

        public override void Start(GameObject go)
        {
            GameObject.Find("GlobalEconomy").SendMessage("RequestConstruction", go.gameObject);
        }

        public override void Update(Inventory inventory, float delta, GameObject go)
        {
            if (hasConstructor) base.Update(inventory, delta, go);
            
            if (inventory.Holdings[AssetType.STATION].Quantity >= 0.9999)
            {
                GameObject.Find("GlobalEconomy").SendMessage("RemoveFromGraph", go.transform.GetComponent<StationBehavior>());
                StationBehavior sb = go.GetComponent<StationBehavior>();
                sb.ChangeStationType(this.StationTypeUnderConstruction);
                sb.underConstruction = false;
                inventory.Holdings[AssetType.STATION].Quantity = 0;
                inventory.Holdings[AssetType.STEEL].Quantity = 0;
            }
        }
        
        public override (List<Order>, List<Order>) MakeOrders(AssetType asset, float lotSize, StationBehavior sb)
        {
            if (StationTypeUnderConstruction != StationType.LOGISTICS) // Logistics stations *must* order from global market
            {
                return base.MakeOrders(asset, lotSize, sb);
            }

            return (new List<Order>(), new List<Order>());
        }

        protected override void BuyAssets(AssetType asset, float lotSize, StationBehavior sb, List<Order> buyOrders)
        {
            // includes pending and completed orders, as building a station is a one-time operation
            float allBuyOrders = sb.Inventory.Holdings[asset].Quantity +
                                 sb.Inventory.Holdings[asset].BuyOrders.Aggregate(0f, (j, k) => j + k.Quantity);
            float numRequired = this.GetWithDefault(Consumes, asset);
            int numOrders = Mathf.CeilToInt((numRequired - allBuyOrders) / lotSize);
            for (int i = 0; i < numOrders; i++)
            {
                try
                {
                    buyOrders.Add(
                        sb.Inventory.Holdings[asset].MakeBuyOrder(lotSize, sb.transform, MarketType.LOCAL)
                    );
                }
                catch (Asset.OrderCapacityReached)
                {
                }
            }
        }

        public override (List<Order>, List<Order>) MakeExternalOrders(AssetType asset, float lotSize, StationBehavior sb)
        {
            List<Order> buyOrders = new List<Order>();
            List<Order> sellOrders = new List<Order>();

            if (StationTypeUnderConstruction == StationType.LOGISTICS)
            {
                float pending = sb.Inventory.Holdings[asset].Quantity +
                                sb.Inventory.Holdings[asset].UndeliveredBuyOrders(MarketType.GLOBAL);
                float numRequired = this.GetWithDefault(Consumes, asset);
                int numOrders = Mathf.CeilToInt((numRequired - pending) / lotSize);
                for (int i = 0; i < numOrders; i++)
                {
                    try
                    {
                        buyOrders.Add(
                            sb.Inventory.Holdings[asset].MakeBuyOrder(lotSize, sb.transform, MarketType.GLOBAL)
                        );
                    }
                    catch (Asset.OrderCapacityReached)
                    {
                    }
                }
            }

            return (buyOrders, sellOrders);
        }
    }

    public class Logistics : Behavior
    {
        public Logistics()
        {
            // TODO make this user defined
            foreach (AssetType a in Enum.GetValues(typeof(AssetType)))
            {
                LocalReserves[a] = EconomicsConstants.Fields.LotSizes[a] * 2f; // the station will keep this amount as standing sell orders to the local economy
                StationReserves[a] = 0f; // the station won't try to buy anything
            }
        }

        public override void Start(GameObject go)
        {
            base.Start(go);

            StationBehavior sb = go.GetComponent<StationBehavior>();

            GameObject er = UnityEngine.Object.Instantiate(sb.EconomicRegionPrefab, go.transform);
            er.name = "EconomicRegion";
            er.transform.localPosition = Vector3.zero;
            GameObject.Find("GlobalEconomy").SendMessage("AddToGraph", sb);
        }

        public override void Destroy(GameObject go)
        {
            base.Destroy(go);
            Destroy(go.transform.Find("EconomicRegion").gameObject); // TODO delete/cancel existing orders
        }

        public override void Update(Inventory inventory, float delta, GameObject go)
        {
            foreach (Collider station in Physics.OverlapSphere(go.transform.position,
                StationConstants.Parameters.LogisticsStationRange,
                UnityConstants.Layers.StationsLayer)
            )
            {
                station.GetComponent<StationBehavior>().LogisticsStation = go.transform;
            }
        }

        /// <summary>
        /// The logistics station does not buy assets from the local economy. They are pushed to it by the stations when
        /// they have a surplus.
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="lotSize"></param>
        /// <param name="sb"></param>
        /// <param name="buyOrders"></param>
        protected override void BuyAssets(AssetType asset, float lotSize, StationBehavior sb, List<Order> buyOrders)
        {
        }

        /// <summary>
        /// The logistics station purchases good from the global network to maintain a stock equivalent to the LocalReserves 
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="lotSize"></param>
        /// <param name="sb"></param>
        /// <returns></returns>
        public override (List<Order>, List<Order>) MakeExternalOrders(AssetType asset, float lotSize, StationBehavior sb)
        {
            // Buy Orders -- buy up to the LocalReserve
            List<Order> buyOrders = new List<Order>();
            List<Order> sellOrders = new List<Order>();

            float quantity = sb.Inventory.Holdings[asset].Quantity;
            float outstandingBuyOrders = sb.Inventory.Holdings[asset].UndeliveredBuyOrders(null);
            float qNewBuys = Mathf.Max(
                this.GetWithDefault(this.LocalReserves, asset) - quantity - outstandingBuyOrders,
                0
            );
            int buyLots = Mathf.FloorToInt(qNewBuys / lotSize);

            for (int i = 0; i < buyLots; i++)
            {
                try
                {
                    buyOrders.Add(
                        sb.Inventory.Holdings[asset].MakeBuyOrder(lotSize, sb.transform, MarketType.GLOBAL)
                    );
                }
                catch (Asset.OrderCapacityReached)
                {
                }
            }

            // Sell Orders -- Same as the normal sell logic, except the logistic station sells go to the global market
            float qAvailable = sb.Inventory.Holdings[asset].Quantity -
                               sb.Inventory.Holdings[asset].UnfilledSellOrders(null) -
                               this.GetWithDefault(this.StationReserves, asset);
            float qTargetLocalSells = this.GetWithDefault(this.LocalReserves, asset) -
                                      sb.Inventory.Holdings[asset].UnfilledSellOrders(MarketType.LOCAL);
            float qActualNewSells = Mathf.Min(qTargetLocalSells, qAvailable);
            float qLogisticsSells = Mathf.Max(qAvailable - qActualNewSells, 0);
            int logisticsSellLots = Mathf.FloorToInt(qLogisticsSells / lotSize);

            for (int i = 0; i < logisticsSellLots; i++)
            {
                try
                {
                    sellOrders.Add(sb.Inventory.Holdings[asset].MakeSellOrder(lotSize, sb.transform, MarketType.GLOBAL)); 
                }
                catch (Asset.OrderCapacityReached)
                {
                }
            
            }

            return (buyOrders, sellOrders);
        }

        /// <summary>
        /// The logistics station does not actively purchase from its local economy, assets are pushed to it from its
        /// members.
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="lotSize"></param>
        /// <param name="sb"></param>
        /// <returns></returns>
        public override (List<Order>, List<Order>) MakeOrders(AssetType asset, float lotSize, StationBehavior sb)
        {
            List<Order> sellOrders = new List<Order>();
            List<Order> buyOrders = new List<Order>();

            SellAssets(asset, lotSize, sb, sellOrders);

            return (buyOrders, sellOrders);
        }

        /// <summary>
        /// Same as the parent class implementation, except the logistics transactions are moved to the MakeExternalOrders
        /// method.
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="lotSize"></param>
        /// <param name="sb"></param>
        /// <param name="sellOrders"></param>
        protected override void SellAssets(AssetType asset, float lotSize, StationBehavior sb,
            List<Order> sellOrders)
        {
            float qAvailable = sb.Inventory.Holdings[asset].Quantity -
                               sb.Inventory.Holdings[asset].UnfilledSellOrders(null) -
                               this.GetWithDefault(this.StationReserves, asset);
            float qTargetLocalSells = this.GetWithDefault(this.LocalReserves, asset) -
                                      sb.Inventory.Holdings[asset].UnfilledSellOrders(MarketType.LOCAL);
            float qActualNewSells = Mathf.Min(qTargetLocalSells, qAvailable);

            int localSellLots = Mathf.FloorToInt(qActualNewSells / lotSize);

            for (int i = 0; i < localSellLots; i++)
            {
                try
                {
                    sellOrders.Add(
                        sb.Inventory.Holdings[asset].MakeSellOrder(lotSize, sb.transform, MarketType.LOCAL)
                    );
                }
                catch (Asset.OrderCapacityReached)
                {
                }
            }
        }
    }

    public class Mine : Behavior
    {
        public Mine()
        {
            Produces[AssetType.IRON_ORE] = 25f;
            StationReserves[AssetType.IRON_ORE] = 0f;
            
            Produces[AssetType.COPPER_ORE] = 5f;
            StationReserves[AssetType.COPPER_ORE] = 0f;
            
            Produces[AssetType.RARE_ORE] = 0.5f;
            StationReserves[AssetType.RARE_ORE] = 0f;
        }
    }

    public class Refinery : Behavior
    {
        public Refinery()
        {
            Produces[AssetType.STEEL] = 25f;
            Consumes[AssetType.IRON_ORE] = 50f;
            StationReserves[AssetType.IRON_ORE] = EconomicsConstants.Fields.LotSizes[AssetType.IRON_ORE] * 1f;
        }
    }

    public class Fabrication : Behavior
    {
        public Fabrication()
        {
            SubTypes = new List<string>(){"HULL", "ENGINE"};
            subType = "HULL";
            
            Consumes[AssetType.STEEL] = 50;
            StationReserves[AssetType.STEEL] = EconomicsConstants.Fields.LotSizes[AssetType.STEEL] * 1f;
        }

        public override void Update(Inventory inventory, float delta, GameObject go)
        {
            Produces.Clear();
            switch (subType)
            {
                case "HULL":
                    Produces[AssetType.HULL] = 10;
                    break;
                
                case "ENGINE":
                    Produces[AssetType.ENGINE] = 1;
                    break;
                
                default:
                    Produces[AssetType.HULL] = 10;
                    break;
            } 
            
            base.Update(inventory, delta, go);
        }
    }

    public class Shipyard : Behavior
    {
        public Shipyard()
        {
            Consumes[AssetType.HULL] = 10;
            StationReserves[AssetType.HULL] = EconomicsConstants.Fields.LotSizes[AssetType.HULL] * 5f;
        }

        public void Build(Transform stationTransform, GameObject buildableObject)
        {
            GameObject.Instantiate(
                buildableObject, 
                stationTransform.position + Vector3.up * 4, 
                Quaternion.identity,
                FrameController.activeFrame.transform // TODO get station's frame instead of active frame
            );
        }
    }

    public class ReceivingPlatform : Logistics
    {
        /* TODO need to impove compatibilty with logistics platforms for this to be turned back on
        public ReceivingPlatform()
        {
            foreach (AssetType a in Enum.GetValues(typeof(AssetType)))
                LocalReserves[a] = Single.PositiveInfinity;
        }
        */
    }
}

namespace Stations
{
    public class Behavior
    {
        public Dictionary<AssetType, float> Consumes = new Dictionary<AssetType, float>();
        public Dictionary<AssetType, float> Produces = new Dictionary<AssetType, float>();

        // Defines the assets reserved for use by a station. Anything over this is available for trade on the local market 
        public Dictionary<AssetType, float> StationReserves = new Dictionary<AssetType, float>();

        // Defines the assets reserved by a logistics station for local use, this is defined separately from station reserves,
        // which take priority over this. Anything over this plus station reserves is available for trade on the logistics
        // network
        public Dictionary<AssetType, float> LocalReserves = new Dictionary<AssetType, float>();

        protected FrameController FrameController;

        // Station sub-types
        public List<String> SubTypes = new List<string>();
        [CanBeNull] public string subType = null;
        
        public virtual void Start(GameObject go)
        {
            FrameController = GameObject.Find("FrameController").GetComponent<FrameController>();
        }

        public virtual void Destroy(GameObject go)
        {
            return;
        }

        public virtual void Update(Inventory inventory, float delta, GameObject go)
        {
            // Make sure we have all of the resources needed to run. Compute the fraction of
            // time delta that the station can consume, if necessary.
            float effectiveDelta = delta;
            foreach (AssetType a in Consumes.Keys)
            {
                if (inventory.Holdings[a].Quantity < delta * Consumes[a])
                {
                    effectiveDelta = Mathf.Min(inventory.Holdings[a].Quantity / Consumes[a], effectiveDelta);
                }
            }

            // Consume and Produce (qty * delta)
            foreach (AssetType a in Consumes.Keys)
            {
                inventory.Holdings[a].Quantity -= Consumes[a] * effectiveDelta;
            }

            foreach (AssetType a in Produces.Keys)
            {
                inventory.Holdings[a].Quantity += Produces[a] * effectiveDelta;
            }
        }

        public virtual (List<Order>, List<Order>) MakeOrders(AssetType asset, float lotSize, StationBehavior sb)
        {
            List<Order> sellOrders = new List<Order>();
            List<Order> buyOrders = new List<Order>();

            // Order matters!
            // MoveSurplusAssetsToLogistics(asset, lotSize, sb);
            SellAssets(asset, lotSize, sb, sellOrders);
            BuyAssets(asset, lotSize, sb, buyOrders);

            return (buyOrders, sellOrders);
        }

        public virtual (List<Order>, List<Order>) MakeExternalOrders(AssetType asset, float lotSize, StationBehavior sb)
        {
            throw new NotImplementedException();
        }

        protected virtual void SellAssets(AssetType asset, float lotSize, StationBehavior sb, List<Order> sellOrders)
        {
            float qAvailable = sb.Inventory.Holdings[asset].Quantity -
                               sb.Inventory.Holdings[asset].UnfilledSellOrders(null) -
                               this.GetWithDefault(this.StationReserves, asset);
            float qTargetLocalSells = this.GetWithDefault(this.LocalReserves, asset) -
                                      sb.Inventory.Holdings[asset].UnfilledSellOrders(MarketType.LOCAL);
            float qActualNewSells = Mathf.Min(qTargetLocalSells, qAvailable);
            float qLogisticsSells = Mathf.Max(qAvailable - qActualNewSells, 0);

            int localSellLots = Mathf.FloorToInt(qActualNewSells / lotSize);
            int logisticsSellLots = Mathf.FloorToInt(qLogisticsSells / lotSize);

            for (int i = 0; i < localSellLots; i++)
            {
                try
                {
                    sellOrders.Add(sb.Inventory.Holdings[asset].MakeSellOrder(lotSize, sb.transform, MarketType.LOCAL));
                }
                catch (Asset.OrderCapacityReached)
                {
                }
            }

            if (sb.LogisticsStation)
            {
                for (int i = 0; i < logisticsSellLots; i++)
                {
                    Order sell;
                    Order buy;
                    try
                    {
                        sell = sb.Inventory.Holdings[asset].MakeSellOrder(lotSize, sb.transform, MarketType.DIRECT);
                        buy = sb.LogisticsStation.GetComponent<StationBehavior>().Inventory.Holdings[asset]
                            .MakeBuyOrder(lotSize, sb.LogisticsStation, MarketType.DIRECT);
                    }
                    catch (Asset.OrderCapacityReached)
                    {
                        continue;
                    }

                    MatchedOrder mo = new MatchedOrder(buy, sell, lotSize);

                    // This order is not added to the SellOrders list, it is instead submitted to the market as a matched order
                    // so that a delivery contract and flight plan can be created.
                    sb.LogisticsStation.Find("EconomicRegion").SendMessage("AddMatchedOrderToQueue", mo);
                }
            }
        }

        protected virtual void BuyAssets(AssetType asset, float lotSize, StationBehavior sb, List<Order> buyOrders)
        {
            float qNewBuys = Mathf.Max(
                this.GetWithDefault(this.StationReserves, asset) -
                sb.Inventory.Holdings[asset].Quantity -
                sb.Inventory.Holdings[asset].UndeliveredBuyOrders(null),
                0);

            int buyLots = Mathf.FloorToInt(qNewBuys / lotSize);
            for (int i = 0; i < buyLots; i++)
            {
                try
                {
                    buyOrders.Add(
                        sb.Inventory.Holdings[asset].MakeBuyOrder(lotSize, sb.transform, MarketType.LOCAL)
                    );
                }
                catch (Asset.OrderCapacityReached)
                {
                }
            }
        }

        protected float GetWithDefault(Dictionary<AssetType, float> d, AssetType asset, float defaultValue = 0f)
        {
            return d.TryGetValue(asset, out var value) ? value : defaultValue;
        }
    }

    public class StationBehavior : MonoBehaviour
    {
        // Construction
        public StationType stationType = StationType.IDLE;
        public bool underConstruction = false;

        // Behavior
        public Inventory Inventory;
        public Behavior Behavior;
        private bool _scuttle;
    
        // Logistics
        public GameObject EconomicRegionPrefab; // The economic region used by logistics stations
        [CanBeNull] public Transform LogisticsStation; // The logistics station that this station can trade with
    

        private Behavior StationTypeToClass(StationType stationType)
        {
            switch (stationType)
            {
                case StationType.ORE_EXTRACTION:
                    return new Mine();
                case StationType.REFINERY:
                    return new Refinery();
                case StationType.FABRICATION:
                    return new Fabrication();
                case StationType.SHIPYARD:
                    return new Shipyard();
                case StationType.LOGISTICS:
                    return new Logistics();
                case StationType.RECEIVING_PLATFORM:
                     return new ReceivingPlatform();
                default:
                    throw new Exception($"Invalid station type {stationType}");
            }
        }

        public void ChangeStationType(StationType newStationType)
        {
            this.stationType = newStationType;
            this.Behavior.Destroy(this.gameObject);
            this.Behavior = this.StationTypeToClass(newStationType);
            this.Behavior.Start(this.gameObject);
        }

        void Start()
        {
            Inventory = new Inventory();

            if (underConstruction)
            {
                Behavior = new UnderConstruction(stationType);
                GameObject.Find("GlobalEconomy").SendMessage("AddToGraph", this);
            }
            else
            {
                Behavior = StationTypeToClass(stationType);
            }
            Behavior.Start(gameObject);
        }

        void Update()
        {
            Inventory.CleanForUpdate();
            if (_scuttle)
            {
                if (CancelOrders())
                {
                    transform.Find("Status Icons").Find("Scuttle").gameObject.SetActive(false);
                    GameObject.Find("EventSystem").SendMessage("deselect", gameObject.GetInstanceID());
                    Destroy(gameObject);
                }
            }
            else
            {
                Behavior.Update(Inventory, Time.deltaTime, gameObject);
            }
        }

        public void ToggleScuttle()
        {
            _scuttle = !_scuttle;
            transform.Find("Status Icons").Find("Scuttle").gameObject.SetActive(_scuttle);
        }
        
        private bool CancelOrders()
        {
            bool allCancelled = true;
            foreach (KeyValuePair<AssetType, Asset> pair in Inventory.Holdings)
            {
                foreach (Order o in pair.Value.BuyOrders)
                {
                    bool isCancelled = o.TryCancelInProgress();
                    if (!isCancelled) allCancelled = false;
                }
            }
            return allCancelled;
        }

        public void Build() // Called by ConstructionFlightControl
        {
            if (Behavior is UnderConstruction)
                ((UnderConstruction) Behavior).hasConstructor = true;
        }
    }
}