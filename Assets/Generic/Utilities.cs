using System;
using UnityEngine;

namespace Generic
{
    public class Utilities
    {
        public class RaycastNotHitException : Exception {
            public RaycastNotHitException(string msg) : base(msg)
            {
            }
        };

        public static Vector3 CursorPositionInWorld(Camera camera, Vector3? normal, Vector3? pos)
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            Plane hPlane = new Plane(normal ?? Vector3.up, pos ?? Vector3.zero);
            if (hPlane.Raycast(ray, out var distance))
            {
                return ray.GetPoint(distance);
            }

            throw new RaycastNotHitException("Ray does not intersect plane");
        }
    }
}