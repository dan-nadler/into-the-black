using System.Collections.Generic;
using Generic.Economics;
using EconomicsConstants;
using UnityEngine;

namespace Generic
{
    namespace Flight
    {
        public enum ShipType {LOCAL_SUPPLY, GLOBAL_SUPPLY, CONSTRUCTION}
        
        public struct CargoAction
        {
            public enum ActionType
            {
                LOAD,
                UNLOAD,
                IDLE
            };

            public ActionType Action;
            public AssetType Asset;
            public float Quantity;

            public CargoAction(AssetType asset, float quantity, ActionType action)
            {
                Asset = asset;
                Quantity = quantity;
                Action = action;
            }

            public CargoAction(ActionType action)
            {
                Action = action;
                Asset = 0;
                Quantity = 0;
            }
        }

        public struct ConstructionAction
        {
            public enum ActionType
            {
                CONSTRUCT,
                IDLE
            }

            public ActionType Action;
            public GameObject ObjectToBuild;

            public ConstructionAction(ActionType action, GameObject objectToBuild)
            {
                Action = action;
                ObjectToBuild = objectToBuild;
            }
        }

        public enum State
        {
            ACCELERATING,
            DECELERATING,
            STATIONKEEPING,
            IDLE
        }

        public class TransportationFlightPlan
        {
            private readonly Queue<(Transform, CargoAction)> _destinations;
            public DeliveryContract DeliveryContract;
            public readonly (Transform, CargoAction)[] OriginalPlan;

            public TransportationFlightPlan(DeliveryContract deliveryContract)
            {
                DeliveryContract = deliveryContract;
                _destinations = new Queue<(Transform, CargoAction)>();

                CargoAction loadAction = new CargoAction(
                    deliveryContract.AssetType, deliveryContract.Quantity, CargoAction.ActionType.LOAD);
                CargoAction unloadAction = new CargoAction(
                    deliveryContract.AssetType, deliveryContract.Quantity, CargoAction.ActionType.UNLOAD);

                _destinations.Enqueue((deliveryContract.Source, loadAction));
                _destinations.Enqueue((deliveryContract.Destination, unloadAction));
                OriginalPlan = _destinations.ToArray();
            }

            public (Transform, CargoAction) Next()
            {
                return _destinations.Dequeue();
            }
        }

        public class ConstructionFlightPlan
        {
            private readonly Queue<(Transform, ConstructionAction)> _destinations;

            public ConstructionFlightPlan(GameObject stationToBuild)
            {
                _destinations = new Queue<(Transform, ConstructionAction)>();
                _destinations.Enqueue(
                    (stationToBuild.transform, new ConstructionAction(
                        ConstructionAction.ActionType.CONSTRUCT, 
                        stationToBuild)
                    )
                );
            }

            public (Transform, ConstructionAction) Next()
            {
                return _destinations.Dequeue();
            }
        }
    }
}
