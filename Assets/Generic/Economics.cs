﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using EconomicsConstants;

namespace Generic
{
    namespace Economics
    {
        public class Order
        {
            public Order(AssetType assetType, float quantity, Transform entity, MarketType market = MarketType.LOCAL)
            {
                AssetType = assetType;
                Quantity = quantity;
                Filled = 0f;
                Entity = entity;
                State = OrderState.OPEN;
                Market = market;
            }

            public AssetType AssetType;
            public float Quantity;
            public float Filled;
            public Transform Entity;
            public OrderState State;
            public MarketType Market;

            public float OutstandingQuantity()
            {
                return this.Quantity - this.Filled;
            }

            public void Fill(float quantity)
            {
                if (quantity == this.Quantity)
                {
                    this.Filled += quantity;
                    this.State = OrderState.FILLED;
                }
                else if (this.Filled > this.Quantity)
                {
                    throw OrderException("More quantity filled than requests.");
                }
                else
                {
                    throw OrderException("Partial fills not implemented.");
                }
            }

            private Exception OrderException(string v)
            {
                throw new Exception(v);
            }

            public void Settle()
            {
                this.State = OrderState.SETTLED;
            }

            public void InTransit()
            {
                this.State = OrderState.IN_TRANSIT;
            }

            /// <summary>
            /// Cancel the order if possible, and return true if successful, or if the order is no longer active.
            /// </summary>
            /// <returns>true if cancelled or inactivate</returns>
            public bool TryCancelInProgress()
            {
                switch (State)
                {
                    case OrderState.OPEN:
                        State = OrderState.CANCELLED;
                        return true;
                    
                    case OrderState.SETTLED:
                        return true; // the order has already been completed and cancelling would have no effect.
                    
                    case OrderState.FILLED:
                        return false; // TODO cancel the flightplan and free up the assigned ship
                    
                    case OrderState.IN_TRANSIT:
                        return false; // TODO reroute the ship to the nearest logistics station to unload the order
                    
                    case OrderState.CANCELLED:
                        return true;
                    
                    default:
                        return false;
                }
            }
        }

        public class Asset
        {
            public Asset(AssetType assetType, float quantity)
            {
                AssetType = assetType;
                Quantity = quantity;
                BuyOrders = new List<Order>();
                SellOrders = new List<Order>();
            }

            public AssetType AssetType;
            public float Quantity;
            public List<Order> BuyOrders;
            public List<Order> SellOrders;
            public int OrderCapacity = 3;
            
            public class OrderCapacityReached : Exception
            {
                public OrderCapacityReached(string message) : base(message)
                {
                }
            }

            public float UndeliveredBuyOrders(MarketType? marketType)
            {
                IEnumerable<Order> bo = FilterBuyOrders(marketType);
                
                float availableQ = 0f;
                foreach (Order o in bo)
                {
                    if (o.State <= OrderState.IN_TRANSIT) // once it's delivered (settled) it's added to inventory
                    {
                        availableQ += o.Quantity;
                    }
                }

                return availableQ;
            }

            public float UnfilledSellOrders(MarketType? marketType)
            {
                IEnumerable<Order> so = FilterSellOrders(marketType);
                
                float availableQ = 0f;
                foreach (Order o in so)
                {
                    // once it's picked up, it's not in inventory and doesn't need to be subtracted
                    if (o.State <= OrderState.FILLED)
                    {
                        availableQ += o.Quantity;
                    }
                }
                return availableQ;
            }
            
            private IEnumerable<Order> FilterSellOrders(MarketType? marketType)
            {
                if (marketType != null) return SellOrders.Where(o => o.Market == marketType);
                
                return SellOrders;
            }
            
            private IEnumerable<Order> FilterBuyOrders(MarketType? marketType)
            {
                if (marketType != null) return BuyOrders.Where(o => o.Market == marketType);
                
                return BuyOrders;
            }

            public void Add(float quantity)
            {
                this.Quantity += quantity;
            }

            public void Remove(float quantity)
            {
                this.Quantity -= quantity;
            }

            public Order MakeBuyOrder(float quantity, Transform buyer, MarketType marketType = MarketType.LOCAL)
            {
                int openOrders = FilterBuyOrders(marketType).Count(o => o.State == OrderState.OPEN);
                if (openOrders >= OrderCapacity)
                {
                    throw new OrderCapacityReached("The entity has too many open orders for this asset.");
                }
                
                Order o = new Order(this.AssetType, quantity, buyer, marketType);
                this.BuyOrders.Add(o);
                return o;
            }

            public Order MakeSellOrder(float quantity, Transform seller, MarketType marketType = MarketType.LOCAL)
            {
                int openOrders = FilterSellOrders(marketType).Count(o => o.State == OrderState.OPEN);
                if (openOrders >= OrderCapacity)
                {
                    throw new OrderCapacityReached("The entity has too many open orders for this asset.");
                }
                
                Order o = new Order(this.AssetType, quantity, seller, marketType);
                this.SellOrders.Add(o);
                return o;
            }

            public void RemoveSettledOrders()
            {
                this.BuyOrders = this.BuyOrders.FindAll(v => v.State != OrderState.SETTLED);
                this.SellOrders = this.SellOrders.FindAll(v => v.State != OrderState.SETTLED);
            }
        }

        public class Inventory
        {
            public Dictionary<AssetType, Asset> Holdings;

            public Inventory(Dictionary<AssetType, Asset> initialHoldings)
            {
                Holdings = initialHoldings;
            }

            /// <summary>
            /// Initialize with 0 holdings for all asset types.
            /// </summary>
            public Inventory()
            {
                this.Holdings = new Dictionary<AssetType, Asset>(Enum.GetNames(typeof(AssetType)).Length);

                foreach (AssetType a in Enum.GetValues(typeof(AssetType)))
                {
                    this.Holdings[a] = new Asset(a, 0);
                }
            }

            public void CleanForUpdate()
            {
                foreach (Asset a in this.Holdings.Values)
                {
                    a.RemoveSettledOrders();
                }
            }
        }

        public struct DeliveryContract
        {
            public DeliveryContract(MatchedOrder matchedOrder)
            {
                MatchedOrder = matchedOrder;
                Source = matchedOrder.Sell.Entity;
                Destination = matchedOrder.Buy.Entity;
                AssetType = matchedOrder.Sell.AssetType;
                Quantity = matchedOrder.Quantity;
            }

            public MatchedOrder MatchedOrder;
            public Transform Source;
            public Transform Destination;
            public AssetType AssetType;
            public float Quantity;
        }

        public class NoOrdersAvailable : Exception
        {
            public NoOrdersAvailable(string message) : base(message)
            {
            }
        }

        public struct MatchedOrder
        {
            public Order Buy;
            public Order Sell;
            public float Quantity;

            public MatchedOrder(Order buy, Order sell, float quantity)
            {
                Buy = buy;
                Sell = sell;
                Quantity = quantity;
            }
        }

        public class RegionalMarket
        {
            public Queue<Order> Buys = new Queue<Order>();  // TODO should this be a different data structure?
            public Queue<Order> Sells = new Queue<Order>();
            
            /// <summary>A naive order clearing algorithm that uses FIFO queues to match orders
            /// irrespective of any parameters like distance or price.</summary>
            public MatchedOrder ClearOrder()
            {
                Order buy;
                Order sell;
                
                // Get the next order from each queue, if necessary. throw an exception if we need a new order, but the
                // queue is empty.
                if (Buys.Count > 0 && Sells.Count > 0)
                {
                    // Dequeue buys and sells until we find an open order. This protects against a case where an order
                    // is cancelled after being submitted.
                    while (Buys.Count > 0 && Buys.Peek().State != OrderState.OPEN) Buys.Dequeue();
                    while (Sells.Count > 0 && Sells.Peek().State != OrderState.OPEN) Sells.Dequeue();

                    if (Buys.Peek().State != OrderState.OPEN || Sells.Peek().State != OrderState.OPEN)
                    {
                        throw new NoOrdersAvailable("Not enough open orders.");
                    }
                    
                    // if the buy and sell orders are from the same entity, requeue the orders.
                    if (Buys.Peek().Entity == Sells.Peek().Entity)
                    {
                        Sells.Enqueue(Sells.Dequeue());
                        Buys.Enqueue(Buys.Dequeue());
                        throw new NoOrdersAvailable("Could not find match.");
                    }
                    
                    buy = Buys.Dequeue();
                    sell = Sells.Dequeue();
                }
                else
                {
                    throw new NoOrdersAvailable("No orders to match.");
                }

                float amountCleared = Mathf.Min(
                    buy.OutstandingQuantity(),
                    sell.OutstandingQuantity()
                );

                buy.Fill(amountCleared);
                sell.Fill(amountCleared);

                return new MatchedOrder(buy, sell, amountCleared);
            }
        }

        public class GlobalMarket : RegionalMarket
        {
        }
    }
}