﻿// TODO move a bunch of shit to this file

using System.Collections.Generic;

namespace StationConstants
{
    public enum StationType
    {
        IDLE, 
        ORE_EXTRACTION,
        REFINERY,
        FABRICATION,
        SHIPYARD,
        LOGISTICS,
        RECEIVING_PLATFORM,
    };

    public static class Parameters
    {
        public static float LogisticsStationRange = 250f;
    }
}

namespace UnityConstants
{
    public static class Layers
    {
        public const int ShipsLayer = 1 << LayerInts.ShipsLayer;
        public const int StationsLayer = 1 << LayerInts.StationsLayer;
    }

    public static class LayerInts
    {
        public const int ShipsLayer = 8;
        public const int StationsLayer = 9;
    }
}

namespace UIConstants
{
    public static class Stations
    {
        public static Dictionary<StationConstants.StationType, string> StationIconLabels = new Dictionary<StationConstants.StationType, string>()
        {
            { StationConstants.StationType.IDLE, "I" },
            { StationConstants.StationType.ORE_EXTRACTION, "M" },
            { StationConstants.StationType.REFINERY, "R" },
            { StationConstants.StationType.FABRICATION, "F" },
            { StationConstants.StationType.SHIPYARD, "S" },
            { StationConstants.StationType.LOGISTICS, "L" },
        };
    }

    public enum StandardSubtypes
    {
        STANDARD
    }
}

namespace EconomicsConstants
{
    public enum MarketType
    {
        LOCAL,
        DIRECT,
        GLOBAL
    }
    
    public enum AssetType
    {
        ORE,
        IRON_ORE,
        COPPER_ORE,
        RARE_ORE,
        STEEL,
        PARTS,
        HULL,
        ENGINE,
        OXYGEN,
        FUEL,
        WATER,
        SUPPLIES,
        CHARGED_BATTERY,
        EMPTY_BATTERY,
        STATION,
    };

    public enum OrderState { OPEN, FILLED, IN_TRANSIT, SETTLED, CANCELLED }

    public static class Fields
    {
        public static Dictionary<AssetType, float> LotSizes = new Dictionary<AssetType, float>()
        {
            { AssetType.ORE, 500f },
            { AssetType.IRON_ORE, 250f },
            { AssetType.COPPER_ORE, 250f },
            { AssetType.RARE_ORE, 100f },
            { AssetType.STEEL, 100f },
            { AssetType.PARTS, 25f },
            { AssetType.HULL, 5f },
            { AssetType.ENGINE, 1f },
            { AssetType.OXYGEN, 100f },
            { AssetType.WATER, 100f },
            { AssetType.FUEL, 100f },
            { AssetType.SUPPLIES, 100f },
            { AssetType.CHARGED_BATTERY, 10f },
            { AssetType.EMPTY_BATTERY, 10f },
            { AssetType.STATION, 1f },
        };
    }
}