using UnityEngine;

namespace Frames
{
    public class FrameController : MonoBehaviour
    {
        public GameObject[] frames = new GameObject[1];
        public GameObject activeFrame;

        private void Start()
        {
            frames[0] = transform.Find("Origin").gameObject;
            activeFrame = frames[0];
        }
    }
}