using System;
using Generic.Flight;
using Stations;

namespace Ships
{
    public class ConstructionShipFlightControl : FlightControl
    {
        public ConstructionFlightPlan FlightPlan;
        public ConstructionAction PendingAction = new ConstructionAction(ConstructionAction.ActionType.IDLE, null);
        
        private void ConstructionBehavior()
        {
            if (state != State.STATIONKEEPING) return;

            if (PendingAction.Action == ConstructionAction.ActionType.CONSTRUCT)
                PendingAction.ObjectToBuild.SendMessage("Build");
        }

        protected override void PostUpdate()
        {
            ConstructionBehavior();
        }

        protected override void PreUpdate()
        {
            if (!this.HasDestination && this.FlightPlan != null)
            {
                try
                {
                    (this.Destination, this.PendingAction) = this.FlightPlan.Next();
                    this.HasDestination = true;
                }
                catch (InvalidOperationException)
                {
                    // no more stops in the flight plan
                    this.HasDestination = false;
                    this.FlightPlan = null;
                    this.Destination = null;
                    this.PendingAction = new ConstructionAction(ConstructionAction.ActionType.IDLE, null);
                    this.state = State.IDLE;
                }
            }
        }
    }
}