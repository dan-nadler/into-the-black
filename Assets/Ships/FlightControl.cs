using Generic.Economics;
using Generic.Flight;
using UnityEngine;

namespace Ships
{
    public class FlightControl : MonoBehaviour
    {
        // Characteristics
        public float ApproachDistance = 10.0f;
        public float Acceleration = 4.0f;
        public float angularVelocityDegPerSec = 15f;
        public ShipType ShipType = ShipType.LOCAL_SUPPLY;

        // Actions
        public Transform Destination = null;
        public bool HasDestination = false;
        public State state = State.IDLE;

        // Flight plan
        public Vector3 Direction = new Vector3();
        private Vector3 PreviousPosition = new Vector3();
        private float rotationStart;
        private float rotationEnd;

        // Inventory
        public Inventory Inventory = new Inventory();

        // Visuals
        public Material EngineHot;
        public Material EngineCold;
        public Renderer[] accelerationPlume;
        public Renderer[] decelerationPlume;
        public Color EmmissionColor = new Color(0, 120, 100, 2.4f);

        private float BrakingDistance()
        {
            return GetComponent<Rigidbody>().velocity.sqrMagnitude / (2.0f * this.Acceleration);
        }

        private Vector3 ComputeDirection(Vector3 destination)
        {
            return (destination - this.transform.position).normalized;
        }

        // For later, when we want to animate 
        private bool FaceDirection(Vector3 direction)
        {
            float angle = Quaternion.Angle(transform.localRotation, Quaternion.LookRotation(direction));
            if (angle < 1f || angle > 355f)
            {
                transform.localRotation = Quaternion.LookRotation(direction);
                return true;
            }
           
            if (rotationEnd < Time.time)
            {
                rotationEnd = Time.time + (angle / angularVelocityDegPerSec);
                rotationStart = Time.time;
            }

            GetComponent<Rigidbody>().rotation = Quaternion.Lerp(
                transform.localRotation, 
                Quaternion.LookRotation(direction),
                (Time.time - rotationStart) / rotationEnd
            );
        
            return false;
        }

        private void SetState(double distance, double previousDistance, double breakingDistance)
        {
            if (this.state == State.IDLE || this.state == State.STATIONKEEPING)
            {
                if (GetComponent<Rigidbody>().velocity.magnitude > 0f)
                {
                    this.state = State.IDLE; // null out velocity before setting new acceleration vector
                }
                else if (this.HasDestination &&
                         distance - breakingDistance > 0.0 &&
                         distance > this.ApproachDistance)
                {
                    this.Direction = this.ComputeDirection(this.Destination.position);
                    this.state = State.ACCELERATING;
                } 
                else if (distance <= this.ApproachDistance)
                {
                    this.state = State.STATIONKEEPING;
                }
            }

            else if (this.state == State.ACCELERATING)
            {
                if (distance - breakingDistance - (ApproachDistance / 2f) <= 0f)
                {
                    this.state = State.DECELERATING;
                }
            }

            else if (this.state == State.DECELERATING)
            {
                if (distance < this.ApproachDistance)
                {
                    this.state = State.STATIONKEEPING;
                } else if (previousDistance < distance) // we've overshot, probably due to framerate and/or precision
                {
                    this.state = State.IDLE; // this will cause the ship to approach again
                }
            }

            else
            {
                this.state = State.IDLE;
            }
        }

        private void SetEngineIntensity()
        {
            switch (this.state)
            {
                case State.ACCELERATING:
                    foreach (Renderer acc in accelerationPlume) acc.material = EngineHot;
                    foreach (Renderer dec in decelerationPlume) dec.material = EngineCold;
                    break;

                case State.DECELERATING:
                    foreach (Renderer acc in accelerationPlume) acc.material = EngineCold;
                    foreach (Renderer dec in decelerationPlume) dec.material = EngineHot;
                    break;

                case State.IDLE:
                    foreach (Renderer acc in accelerationPlume) acc.material = EngineCold;
                    foreach (Renderer dec in decelerationPlume) dec.material = EngineCold;
                    break;

                case State.STATIONKEEPING:
                    foreach (Renderer acc in accelerationPlume) acc.material = EngineCold;
                    foreach (Renderer dec in decelerationPlume) dec.material = EngineCold;
                    break;

                default:
                    foreach (Renderer acc in accelerationPlume) acc.material = EngineCold;
                    foreach (Renderer dec in decelerationPlume) dec.material = EngineCold;
                    break;
            }
        }

        private void FlightBehavior(float delta)
        {
            Rigidbody r = GetComponent<Rigidbody>();
            switch (this.state)
            {
                case State.ACCELERATING:
                    if (this.FaceDirection(this.Direction)) // wait until facing the correct direction
                        r.velocity += Acceleration * delta * Direction.normalized;
                    break;

                case State.DECELERATING:
                    r.velocity -= Acceleration * delta * Direction.normalized;
                    break;

                case State.IDLE:
                case State.STATIONKEEPING:
                    // if we're above 1/10th of acceleration, correct velocity with engines, otherwise just set it to zero
                    if (r.velocity.magnitude > this.Acceleration / 10.0f)  
                    {
                        r.velocity -= this.Acceleration * delta * Direction.normalized;
                        if (r.velocity.magnitude < 0f)
                        {
                            r.velocity = Vector3.zero;
                        }
                    }
                    else
                    {
                        r.velocity = Vector3.zero;
                    }

                    break;
            }
        }
        
        // Start is called before the first frame update
        void Start()
        {
            this.state = State.IDLE;
        }

        protected virtual void PreUpdate(){}
        protected virtual void PostUpdate(){}

        // Update is called once per frame
        void Update()
        {
            this.Inventory.CleanForUpdate();
            float delta = Time.deltaTime;
            PreUpdate();
            
            Vector3 dest;
            if (!this.Destination)
            {
                dest = this.transform.position;
            }
            else
            {
                dest = this.Destination.position;
            }

            double previousDistance = (PreviousPosition - dest).magnitude;
            double distance = (this.transform.position - dest).magnitude;
            this.SetState(distance, previousDistance, this.BrakingDistance());
            this.FlightBehavior(delta);
            this.SetEngineIntensity();
            PostUpdate();
        }
    }
}
