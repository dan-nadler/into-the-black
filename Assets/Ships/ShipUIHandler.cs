using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShipUIHandler : MonoBehaviour
{
    private Image SelectionCircle;
    public bool Selected = false;
    public float UnitScale = 160f;

    void Start()
    {
        SelectionCircle = transform.Find("Selected").GetComponent<Image>();
    }

    private Vector3 ComputeScale()
    {
        
        float distanceToCamera = Mathf.Abs((Camera.main.transform.position - transform.position).magnitude);
        float scale = distanceToCamera / UnitScale;
        return new Vector3(scale, scale, scale);
    }

    void Update()
    {
        transform.LookAt(Camera.main.transform);
        transform.localScale = ComputeScale();
    }

    //private void OnMouseDown()
    //{
    //    Selected = !Selected;
    //    SelectionCircle.enabled = Selected;
    //}
}
