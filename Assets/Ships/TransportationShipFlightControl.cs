using System;
using Generic.Flight;
using Stations;

namespace Ships
{
    public class TransportationShipFlightControl : FlightControl
    {
        public TransportationFlightPlan FlightPlan;
        public CargoAction PendingAction = new CargoAction(CargoAction.ActionType.IDLE);
        
        private void CargoBehavior()
        {
            if (this.state != State.STATIONKEEPING) return;

            if (this.PendingAction.Action == CargoAction.ActionType.LOAD)
            {
                this.FlightPlan.DeliveryContract.Source
                    .GetComponent<StationBehavior>().Inventory
                    .Holdings[this.PendingAction.Asset]
                    .Remove(this.PendingAction.Quantity);

                this.Inventory.Holdings[PendingAction.Asset].Add(this.PendingAction.Quantity);
                this.HasDestination = false;

                this.FlightPlan.DeliveryContract.MatchedOrder.Buy.InTransit();
                this.FlightPlan.DeliveryContract.MatchedOrder.Sell.InTransit();
            }

            if (this.PendingAction.Action == CargoAction.ActionType.UNLOAD)
            {
                this.FlightPlan.DeliveryContract.Destination
                    .GetComponent<StationBehavior>().Inventory
                    .Holdings[this.PendingAction.Asset]
                    .Add(this.PendingAction.Quantity);

                this.Inventory.Holdings[PendingAction.Asset].Remove(this.PendingAction.Quantity);
                this.HasDestination = false;

                this.FlightPlan.DeliveryContract.MatchedOrder.Buy.Settle();
                this.FlightPlan.DeliveryContract.MatchedOrder.Sell.Settle();
            }
        }

        protected override void PostUpdate()
        {
            CargoBehavior();
        }

        protected override void PreUpdate()
        {
            if (!this.HasDestination && this.FlightPlan != null)
            {
                try
                {
                    (this.Destination, this.PendingAction) = this.FlightPlan.Next();
                    this.HasDestination = true;
                }
                catch (InvalidOperationException)
                {
                    // no more stops in the flight plan
                    this.HasDestination = false;
                    this.FlightPlan = null;
                    this.Destination = null;
                    this.PendingAction = new CargoAction(CargoAction.ActionType.IDLE);
                    this.state = State.IDLE;
                }
            }
        }
    }
}