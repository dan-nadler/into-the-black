using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class ProcedualAsteroidField : MonoBehaviour
{
    public int Seed = 712817234;
    public Transform[] Asteroids;
    private System.Random rand;

    // Start is called before the first frame update
    void Start()
    {
        this.rand = new System.Random(this.Seed);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private Vector3 FieldSize()
    {
        float x = (float)this.rand.Next(20, 300);
        float y = (float)this.rand.Next(20, 50);
        float z = (float)this.rand.Next(20, 300);

        return new Vector3(x, y, z);
    }

    private float AsteroidDensity()
    {
        return (float)this.rand.Next(80, 100);
    }

    private void GenerateAsteroids()
    {
        Vector3 fieldSize = this.FieldSize();
        float density = AsteroidDensity();

        for (int x = 0; x <= (int)fieldSize.x; x++)
        {
            for (int y = 0; y <= (int)fieldSize.y; y++)
            {
                for (int z = 0; z <= (int)fieldSize.z; z++)
                {
                    if (this.rand.Next(1, 100) > density)
                    {
                        
                    }
                }
            }
        }
    }
}
