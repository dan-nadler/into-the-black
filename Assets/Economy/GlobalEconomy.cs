﻿using System;
using System.Collections;
using System.Collections.Generic;
using EconomicsConstants;
using Generic.DataStructures;
using Generic.Economics;
using Generic.Flight;
using Ships;
using Stations;
using UnityEngine;

namespace Economy
{
    public class GlobalEconomy : MonoBehaviour
    {
        public Graph<StationBehavior> ZoneGraph = new Graph<StationBehavior>();
        public Dictionary<StationBehavior, GraphNode<StationBehavior>> NodeDict =
            new Dictionary<StationBehavior, GraphNode<StationBehavior>>();

        private Dictionary<AssetType, GlobalMarket> GlobalMarkets = new Dictionary<AssetType, GlobalMarket>();
        public Queue<MatchedOrder> MatchedOrders = new Queue<MatchedOrder>();
        public ShipType AssignedShipType = ShipType.GLOBAL_SUPPLY;
        private float ShipRadius = Single.PositiveInfinity;

        private readonly Queue<GameObject> _stationsToConstruct = new Queue<GameObject>();

        void Start()
        {
            foreach (AssetType asset in Enum.GetValues(typeof(AssetType)))
            {
                this.GlobalMarkets[asset] = new GlobalMarket();
            }

            StartCoroutine(ProcessStationNodes());
            StartCoroutine(MatchOrders());
            StartCoroutine(AssignTransportShips());
            StartCoroutine(AssignConstructionShips());
        }

        void Update()
        {
        }

        private IEnumerator AssignConstructionShips()
        {
            while (true)
            {
                // Update available ships -- hard to optimize b/c ships can move in and out of range.
                Queue<ConstructionShipFlightControl> availableShips = QueueAvailableConstructionShips();
                
                while (availableShips.Count > 0 && _stationsToConstruct.Count > 0)
                {
                    ConstructionShipFlightControl ship = availableShips.Dequeue();
                    GameObject station = _stationsToConstruct.Dequeue();
                    ConstructionFlightPlan cfp = new ConstructionFlightPlan(station);
                    ship.FlightPlan = cfp;
                    yield return null;
                }

                yield return true;
            }
        }
        
        private IEnumerator AssignTransportShips()
        {
            while (true)
            {
                // Update available ships -- hard to optimize b/c ships can move in and out of range.
                Queue<TransportationShipFlightControl> availableShips = QueueAvailableTransportShips();

                // Assign available ships to matched orders
                while (MatchedOrders.Count > 0 && availableShips.Count > 0)
                {
                    DeliveryContract deliveryContract = new DeliveryContract(MatchedOrders.Dequeue());
                    TransportationFlightPlan flightPlan = new TransportationFlightPlan(deliveryContract);
                    availableShips.Dequeue().FlightPlan = flightPlan;
                    yield return null;
                }
                yield return null;
            }
        }
        
        private IEnumerator MatchOrders()
        {
            while(true)
            {
                foreach (AssetType assetType in this.GlobalMarkets.Keys)
                {
                    try
                    {
                        MatchedOrders.Enqueue(this.GlobalMarkets[assetType].ClearOrder());
                    }
                    catch (NoOrdersAvailable)
                    {
                        continue;
                    }
                }
                yield return null;
            }
        }

        private IEnumerator ProcessStationNodes()
        {
            while(true)
            {
                if (ZoneGraph.Count == 0) yield return null;

                // Use a queue to avoid issues with mutating the collection while iterating over it. This will update
                // the stations queue after each full pass through the graph.
                Queue<StationBehavior> stationQueue = new Queue<StationBehavior>();
                foreach (Node<StationBehavior> stationNode in ZoneGraph.Nodes) stationQueue.Enqueue(stationNode.Value);

                while (stationQueue.Count > 0)
                {
                    StationBehavior station = stationQueue.Dequeue();

                    foreach (AssetType asset in Enum.GetValues(typeof(AssetType)))
                    {
                        (List<Order> buyOrders, List<Order> sellOrders) =
                            station.Behavior.MakeExternalOrders(asset, Fields.LotSizes[asset], station);

                        foreach (Order b in buyOrders)
                        {
                            GlobalMarkets[asset].Buys.Enqueue(b);
                        }

                        foreach (Order s in sellOrders)
                        {
                            GlobalMarkets[asset].Sells.Enqueue(s);
                        }
                    }

                    yield return null;
                }
            }
        }

        private Queue<TransportationShipFlightControl> QueueAvailableTransportShips()
        {
            Queue<TransportationShipFlightControl> availableShips = new Queue<TransportationShipFlightControl>();
            foreach (Collider ship in Physics.OverlapSphere(this.transform.position, this.ShipRadius,
                UnityConstants.Layers.ShipsLayer))
            {
                TransportationShipFlightControl fc = ship.GetComponent<TransportationShipFlightControl>();
                if (fc && fc.ShipType == this.AssignedShipType && fc.FlightPlan == null)
                {
                    availableShips.Enqueue(fc);
                }
            }
            return availableShips;
        }
        
        private Queue<ConstructionShipFlightControl> QueueAvailableConstructionShips()
        {
            Queue<ConstructionShipFlightControl> availableShips = new Queue<ConstructionShipFlightControl>();
            foreach (Collider ship in Physics.OverlapSphere(this.transform.position, this.ShipRadius,
                UnityConstants.Layers.ShipsLayer))
            {
                ConstructionShipFlightControl fc = ship.GetComponent<ConstructionShipFlightControl>();
                if (fc && fc.ShipType == ShipType.CONSTRUCTION && fc.FlightPlan == null)
                {
                    availableShips.Enqueue(fc);
                }
            }
            return availableShips;
        }

        
        /// <summary>
        /// Called by stations via SendMessage to request a construction ship.
        /// </summary>
        /// <param name="go"></param>
        public void RequestConstruction(GameObject go)
        {
            _stationsToConstruct.Enqueue(go);
        }

        /// <summary>
        /// Adds an Economy monobehavior to the graph. Called via SendMessage by Economy.Start()
        /// </summary>
        /// <param name="e"></param>
        void AddToGraph(StationBehavior sb)
        {
            Debug.Log($"Adding {sb.name} to logistics graph.");
            GraphNode<StationBehavior> gn = new GraphNode<StationBehavior>(sb);
            if (!this.NodeDict.ContainsKey(sb))
            {
                this.NodeDict[sb] = gn;
                ZoneGraph.AddNode(sb);
            }
        }
    
        void RemoveFromGraph(StationBehavior sb)
        {
            Debug.Log($"Removing {sb.name} from logistics graph.");
            // GraphNode<StationBehavior> gn = new GraphNode<StationBehavior>(sb);
            ZoneGraph.Remove(sb);
            if (this.NodeDict.ContainsKey(sb)) this.NodeDict.Remove(sb);
        }

        void AddEdge(StationBehavior from, StationBehavior to, int currentStrength)
        {
            ZoneGraph.AddDirectedEdge(this.NodeDict[from], this.NodeDict[to], 1);
        }
    }
}