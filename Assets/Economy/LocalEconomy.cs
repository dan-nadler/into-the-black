﻿using System;
using System.Collections;
using System.Collections.Generic;
using EconomicsConstants;
using Generic.Economics;
using Generic.Flight;
using Ships;
using Stations;
using UnityEngine;

namespace Economy
{
    /// <summary>
    /// This is automatically added to a Logistics station as part of the EconomicRegion prefab.
    /// The StationBehavior initialization handles this if the StationType is Logistics.
    /// </summary>
    public class LocalEconomy : MonoBehaviour
    {
        public Queue<StationBehavior> Stations = new Queue<StationBehavior>();
        private Dictionary<AssetType, RegionalMarket> Markets = new Dictionary<AssetType, RegionalMarket>();
        public Queue<MatchedOrder> MatchedOrders = new Queue<MatchedOrder>();
        public ShipType AssignedShipType = ShipType.LOCAL_SUPPLY;
        public float Radius = StationConstants.Parameters.LogisticsStationRange;
    
        public void AddStation(StationBehavior station)
        {
            if (!Stations.Contains(station)) Stations.Enqueue(station);
        }

        void Start()
        {
            foreach (AssetType asset in Enum.GetValues(typeof(AssetType)))
                this.Markets[asset] = new RegionalMarket();

            StartCoroutine(ProcessStations());
            StartCoroutine(MatchOrders());
            StartCoroutine(AssignShips());
        }

        private IEnumerator ProcessStations()
        {
            while (true)
            {
                if (Stations.Count == 0) yield return null;
            
                // get the next station for processing, and add it to the end of the queue
                StationBehavior station = Stations.Dequeue(); 
                Stations.Enqueue(station);
            
                foreach (AssetType asset in Enum.GetValues(typeof(AssetType)))
                {
                    (List<Order> buyOrders, List<Order> sellOrders) =
                        station.Behavior.MakeOrders(asset, Fields.LotSizes[asset], station);

                    foreach (Order b in buyOrders)
                    {
                        this.Markets[asset].Buys.Enqueue(b);
                    }

                    foreach (Order s in sellOrders)
                    {
                        this.Markets[asset].Sells.Enqueue(s);
                    }
                }
                yield return null;
            }
        }

        private IEnumerator MatchOrders()
        {
            while (true)
            {
                // Add matched orders to delivery queue
                foreach (AssetType assetType in this.Markets.Keys)
                {
                    try
                    {
                        MatchedOrders.Enqueue(this.Markets[assetType].ClearOrder());
                    }
                    catch (NoOrdersAvailable)
                    {
                        continue;
                    }
                }

                yield return null;
            }
        }

        private IEnumerator AssignShips()
        {
            while (true)
            {
                // Update available ships -- hard to optimize b/c ships can move in and out of range.
                Queue<TransportationShipFlightControl> availableShips = QueueAvailableShips();

                // Assign available ships to matched orders
                while (MatchedOrders.Count > 0 && availableShips.Count > 0)
                {
                    DeliveryContract deliveryContract = new DeliveryContract(MatchedOrders.Dequeue());
                    TransportationFlightPlan flightPlan = new TransportationFlightPlan(deliveryContract);
                    availableShips.Dequeue().FlightPlan = flightPlan;
                    yield return null;
                }

                yield return null;
            }
        }
    
        void Update()
        {
            // Update stations in range -- do this only when stations are created?
            foreach (Collider station in Physics.OverlapSphere(this.transform.position, this.Radius,
                UnityConstants.Layers.StationsLayer))
            {
                StationBehavior sb = station.GetComponent<StationBehavior>();
                if (sb) this.AddStation(sb);
            }
        }

        private Queue<TransportationShipFlightControl> QueueAvailableShips()
        {
            Queue<TransportationShipFlightControl> AvailableShips = new Queue<TransportationShipFlightControl>();
            foreach (Collider ship in Physics.OverlapSphere(this.transform.position, this.Radius,
                UnityConstants.Layers.ShipsLayer))
            {
                TransportationShipFlightControl fc = ship.GetComponent<TransportationShipFlightControl>();
                if (fc && fc.ShipType == this.AssignedShipType && fc.FlightPlan == null)
                {
                    AvailableShips.Enqueue(fc);
                }
            }

            return AvailableShips;
        }

        /// <summary>
        /// Receives 'push' orders from local stations.
        /// </summary>
        /// <param name="mo"></param>
        public void AddMatchedOrderToQueue(MatchedOrder mo)
        {
            Debug.Log("Adding matched order via message");
            this.MatchedOrders.Enqueue(mo);
        }
    }
}