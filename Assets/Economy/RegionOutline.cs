using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegionOutline : MonoBehaviour
{

    public Transform XZ;
    public Transform XY;
    public Transform YZ;
    public Transform Volume;
    public float Radius;

    private float LerpTime = 0f;
    public float AnimationLength = 1f;
    public bool Visible;

    private Quaternion selfStart;
    private Quaternion XZstart;
    private Quaternion XYstart;
    private Quaternion YZstart;
    private Vector3 volumeStart;

    void OnStartAnimation()
    {
        selfStart = Quaternion.identity;
        XZstart = Quaternion.identity;
        XYstart = Quaternion.identity;
        YZstart = Quaternion.identity;
        volumeStart = Vector3.one;

        XZ.gameObject.SetActive(true);
        XY.gameObject.SetActive(true);
        YZ.gameObject.SetActive(true);
        Volume.gameObject.SetActive(true);

        transform.rotation = selfStart;
        XZ.rotation = XZstart;
        XY.rotation = XYstart;
        YZ.rotation = YZstart;
        Volume.transform.localScale = volumeStart;
    }

    void Update()
    {
        if (Visible)
        {
            if (LerpTime == 0) OnStartAnimation();

            if (LerpTime < AnimationLength)
            {
                LerpTime += Time.deltaTime;
                RotateLines();
            }
        } else
        {
            LerpTime = 0f;
            XZ.gameObject.SetActive(false);
            XY.gameObject.SetActive(false);
            YZ.gameObject.SetActive(false);
            Volume.gameObject.SetActive(false);
        }
        
    }

    void RotateLines()
    {
        float lerpTime = Mathf.Min(LerpTime, AnimationLength) / AnimationLength;

        Quaternion r = Quaternion.AngleAxis(90, Vector3.up);
        transform.localRotation = Quaternion.Lerp(selfStart, r, lerpTime);

        Quaternion r0 = Quaternion.AngleAxis(90, Vector3.forward);
        XY.localRotation = Quaternion.Lerp(XYstart, r0, lerpTime);

        Quaternion r1 = Quaternion.AngleAxis(90, Vector3.up);
        XZ.localRotation = Quaternion.Lerp(XZstart, r1, lerpTime);

        Quaternion r2 = Quaternion.AngleAxis(90, Vector3.right);
        YZ.localRotation = Quaternion.Lerp(YZstart, r2, lerpTime);

        Vector3 v = new Vector3(2 * Radius, 2 * Radius, 2 * Radius);
        Volume.localScale = Vector3.Lerp(volumeStart, v, lerpTime);
    }
}
