﻿using UnityEngine;

namespace CelestialBodies.Sun.Scripts_Shaders
{
	public class LookAtScript : MonoBehaviour {

		public bool LockScale = false;
		public Vector3 UnitScale;
	
		private Vector3 ComputeScale()
		{
			float distanceToCamera = Mathf.Abs((Camera.main.transform.position - transform.position).magnitude);
			float x = distanceToCamera / UnitScale.x;
			float y = distanceToCamera / UnitScale.y;
			float z = distanceToCamera / UnitScale.z;
			return new Vector3(x, y, z);
		}

		void Update () {
			this.transform.LookAt(Camera.main.transform.position);

			if (LockScale)
			{
				this.transform.localScale = ComputeScale();
			}
		}
	}
}
